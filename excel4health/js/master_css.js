// -- clients can use this jQuery instance ll(), but do not remove
var ykdebug = false;
var ll = jQuery.noConflict();
var currencySymbol = '$';
var internal = 0;
var payment_methods = new Array('2', '4', '5', '6', '7', '8', '9', '10', '13', '19');
var ll_system_ids = new Array('1', '4', '7');
var bam_system_ids = new Array('10', '13');
var content_type_text = "application/json; charset=utf-8";

function submitStep1Cart() {
    var offer_id = ll("#offer_id").val();
    var step = 1;
    if (!lockButton('submitStep1Cart', 1)) {
        return false;
    }
    if (typeof ll("#step").val() != "undefined") {
        step = ll("#step").val();
    }

    var country = "";
    if (typeof ll("#country").val() != "undefined") {
        country = ll("#country").val();
    }

    internal = 1;
    var labels = [];
    var validate_fields = [];
    var validate_fields_labels = [];

    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=" + step + "&offer_id=" + offer_id + "&country=" + country,
        success: function (data) {
            ll("#rush_order_btn1").html("<span id='process'>" + data.processing_css + "</span>");
            ll.each(data.language, function (i, obj) {
                labels.push([obj]);
            });

            ll.each(data.valid_fields, function (i, obj) {
                validate_fields.push([obj]);
            });

            ll.each(data.valid_field_names, function (i, obj) {
                validate_fields_labels.push([obj]);
            });

            submitStep1CartSuccess(labels, validate_fields, validate_fields_labels, offer_id);

        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitStep1Cart', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
        }
    });
}

function submitStep1CartSuccess(labels, validate_fields, validate_fields_labels, offer_id) {

    if (form_validator(labels, validate_fields, validate_fields_labels) == true) {

        var url = application_url + "/campaigns/ykprocesser.php?callback=?";

        var ykVars = "";
        var inputs = ll('#opt_in_form').serializeArray();

        ll.each(inputs, function (i, field) {
            if (field.name != "step") {
                ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
            }
        });

        ll.ajax({
            type: 'POST',
            url: url,
            async: false,
            jsonpCallback: 'processCallBack',
            contentType: content_type_text,
            dataType: 'jsonp',
            data: ll('#opt_in_form').serializeArray(),
            success: function (data) {

                if (data.response.status == "success") {
                    window.location = data.response.secure_url
                            + "cart.php?" + ykVars + "&prospectId="
                            + data.response.prospect_id;
                } else {
                    var msg = data.response.fields.error;
                    alert("Error: " + msg);
                    lockButton('submitStep1Cart', 2);
                    ll("#rush_order_btn1").html("<span id='process'>" + data.processing_rush + "</span>");
                }
            },
            error: function (e) {
                alert("Network Error Try again Later");
                lockButton('submitStep1Cart', 2);
                ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
            }
        });
    } else {
        lockButton('submitStep1Cart', 2);
        ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
    }
}

function submitPartnerUpsell() {
    var url = application_url + "/campaigns/partner.php?callback=?";
    ll("#add_elite").hide();
    ll("#process").show();
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: ll("#opt_in_form10").serializeArray(),
        success: function (data) {
            if (data.response.status == "100") {
                ll(".elite_upsell").html(data.response.html);
            } else {
                ll(".elite_upsell").html(data.response.membership_id);
            }
        },
        error: function (e) {
            // do nothing
        }
    });
}

function submitStep1Address() {
    var offer_id = ll("#offer_id").val();
    var step = 1;
    if (!lockButton('submitStep1Address', 1)) {
        return false;
    }

    if (typeof ll("#step").val() != "undefined") {
        step = ll("#step").val();
    }
    var country = "";
    if (typeof ll("#country").val() != "undefined") {
        country = ll("#country").val();
    }
    var labels = [];
    var validate_fields = [];
    var validate_fields_labels = [];

    internal = 1;
    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=" + step + "&offer_id=" + offer_id + "&country=" + country,
        success: function (data) {
            ll('#rush_order_btn1').html('<span id="process">' + data.processing_css + '</span>');
            ll.each(data.language, function (i, obj) {
                labels.push([obj]);
            });

            ll.each(data.valid_fields, function (i, obj) {
                validate_fields.push([obj]);
            });

            ll.each(data.valid_field_names, function (i, obj) {
                validate_fields_labels.push([obj]);
            });
            submitStep1AddressSuccess(labels, validate_fields, validate_fields_labels, offer_id);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitStep1Address', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
        }
    });
}

function exitUpdateShipping() {
    internal = 1;
    var url = application_url + "/campaigns/shipping.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: ll('#opt_in_form').serializeArray(),
        success: function (data) {
        },
        error: function (e) {
            alert("Network Error Try again Later");
        }
    });
}

function submitStep1AddressSuccess(labels, validate_fields, validate_fields_labels, offer_id) {
    if (form_validator(labels, validate_fields, validate_fields_labels) == true) {

        var url = application_url + "/campaigns/ykprocesser.php?callback=?";

        var ykVars = "";
        var inputs = ll('#opt_in_form').serializeArray();

        ll.each(inputs, function (i, field) {
            if (field.name != "step") {
                ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
            }
        });

        ll.ajax({
            type: 'POST',
            url: url,
            async: false,
            jsonpCallback: 'processCallBack',
            contentType: content_type_text,
            dataType: 'jsonp',
            data: ll('#opt_in_form').serializeArray(),
            success: function (data) {

                if (data.response.status == "success") {
                    window.location = data.response.secure_url
                            + "address.php?" + ykVars + "&prospectId="
                            + data.response.prospect_id;
                } else {
                    var msg = data.response.fields.error;
                    alert("Error: " + msg);
                    lockButton('submitStep1Address', 2);
                    ll("#rush_order_btn1").html("<span id='process'>" + data.processing_rush + "</span>");
                }
            },
            error: function (e) {
                alert("Network Error Try again Later");
                lockButton('submitStep1Address', 2);
                ll("#rush_order_btn1").html("<span id='process'>" + data.processing_rush + "</span>");
            }
        });
    }
}

function submitStep1CartLocalLang() {

    var offer_id = ll("#offer_id").val();
    var step = 1;
    if (!lockButton('submitStep1CartLocalLang', 1)) {
        return false;
    }

    if (typeof ll("#step").val() != "undefined") {
        step = ll("#step").val();
    }

    var country = "";
    if (typeof ll("#country").val() != "undefined") {
        country = ll("#country").val();
    }

    internal = 1;
    var labels = [];
    var validate_fields = [];
    var validate_fields_labels = [];

    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=" + step + "&offer_id=" + offer_id + "&country=" + country,
        success: function (data) {
            ll.each(data.language, function (i, obj) {
                labels.push([obj]);
            });

            ll.each(data.valid_fields, function (i, obj) {
                validate_fields.push([obj]);
            });

            ll.each(data.valid_field_names, function (i, obj) {
                validate_fields_labels.push([obj]);
            });
            submitCartLocalSuccess(labels, validate_fields,
                    validate_fields_labels, offer_id);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitStep1CartLocalLang', 2);
            ll("#process").html(rush_label);
        }
    });

}

function submitCartLocalSuccess(labels, validate_fields, validate_fields_labels, offer_id) {
    if (form_validator(labels, validate_fields, validate_fields_labels) == true) {
        var url = application_url + "/campaigns/ykprocesser.php?callback=?";

        var ykVars = "";
        var inputs = ll('#opt_in_form').serializeArray();

        ll.each(inputs, function (i, field) {
            if (field.name != "step") {
                ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
            }
        });

        ll.ajax({
            type: 'POST',
            url: url,
            async: false,
            jsonpCallback: 'processCallBack',
            contentType: content_type_text,
            dataType: 'jsonp',
            data: ll('#opt_in_form').serializeArray(),
            success: function (data) {

                if (data.response.status == "success") {
                    window.location = data.response.secure_url + "cart.php?"
                            + ykVars + "&prospectId="
                            + data.response.prospect_id;
                } else {
                    var msg = data.response.fields.error;
                    alert("Error: " + msg);
                    lockButton('submitStep1CartLocalLang', 2);
                    ll("#process").html(rush_label);
                }
            },
            error: function (e) {
                alert("Network Error Try again Later");
                lockButton('submitStep1CartLocalLang', 2);
                ll("#process").html(rush_label);
            }
        });
    }
}

function submitStep1() {
    ykHook('SubmitBegin');

    if (!lockButton('submitStep1', 1)) {
        return false;
    }

    ll('.exit_pop_header').hide();
    var offer_id = ll("#offer_id").val();

    internal = 1;
    var step = 1;
    if (typeof ll("#step").val() != "undefined") {
        step = ll("#step").val();
    }
    var country = "";
    if (typeof ll("#country").val() != "undefined") {
        country = ll("#country").val();
    }

    var labels = [];
    var validate_fields = [];
    var validate_fields_labels = [];

    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=" + step + "&offer_id=" + offer_id + "&country=" + country,
        success: function (data) {
            ll("#rush_order_btn1").html("<span id='process'>" + data.processing_css + "</span>");
            //ll("#submit-link").html("<div class=\"rush-order\" id=\"rush_order_btn1\"> <span id=\"process\">" + data.processing_css + "</span></div>");
            ll.each(data.language, function (i, obj) {
                labels.push([obj]);
            });
            ll.each(data.valid_fields, function (i, obj) {
                validate_fields.push([obj]);
            });
            ll.each(data.valid_field_names, function (i, obj) {
                validate_fields_labels.push([obj]);
            });
            submitStepOneSuccess(labels, validate_fields, validate_fields_labels, offer_id);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitStep1', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
            ykHook('SubmitEnd');
        }
    });
}

function submitStepOneSuccess(labels, validate_fields, validate_fields_labels, offer_id) {
    if (form_validator(labels, validate_fields, validate_fields_labels) == true) {

        var url = application_url + "/campaigns/ykprocesser.php?callback=?";
        var ykVars = "";
        var inputs = ll('#opt_in_form').serializeArray();

        ll.each(inputs, function (i, field) {
            if (ykVars) {
                ykVars += "&";
            }
            ykVars += field.name + "=" + encodeURIComponent(field.value);
        });

        ll.ajax({
            type: 'POST',
            url: url,
            async: false,
            jsonpCallback: 'processCallBack',
            contentType: content_type_text,
            dataType: 'jsonp',
            data: ll('#opt_in_form').serializeArray(),
            success: function (data) {
                if (data.response.status == "success") {
                    window.location = data.response.secure_url
                            + "order.php?" + ykVars + "&prospectId="
                            + data.response.prospect_id;
                } else {
                    var msg = data.response.fields.error;
                    alert("Error: " + msg);
                    lockButton('submitStep1', 2);
                    ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
                }
                ykHook('SubmitEnd');
            },
            error: function (e) {
                alert("Network Error Try again Later");
                lockButton('submitStep1', 2);
                ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
                ykHook('SubmitEnd');
            }
        });
    } else {
        lockButton('submitStep1', 2);
        ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
        ykHook('SubmitEnd');
    }
}

function submitStep1LocalLang() {

    if (!lockButton('submitStep1LocalLang', 1)) {
        return false;
    }

    var offer_id = ll("#offer_id").val();
    internal = 1;

    var labels = [];
    var validate_fields = [];
    var validate_fields_labels = [];

    var country = "";
    if (typeof ll("#country").val() != "undefined") {
        country = ll("#country").val();
    }

    ll("#process").html(process_label);
    //ll("#submit-link-a").attr('onclick', ''); // locking button now instead.

    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=1&offer_id=" + offer_id + "&country=" + country,
        success: function (data) {

            ll.each(data.language, function (i, obj) {
                labels.push([obj]);
            });

            ll.each(data.valid_fields, function (i, obj) {
                validate_fields.push([obj]);
            });

            ll.each(data.valid_field_names, function (i, obj) {
                validate_fields_labels.push([obj]);
            });
            submitOneLocalSucess(labels, validate_fields, validate_fields_labels, offer_id);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitStep1LocalLang', 2);
            ll("#process").html(rush_label);
        }
    });

}

function submitOneLocalSucess(labels, validate_fields, validate_fields_labels, offer_id) {
    if (form_validator(labels, validate_fields, validate_fields_labels) == true) {
        var url = application_url + "/campaigns/ykprocesser.php?callback=?";
        var ykVars = "";
        var inputs = ll('#opt_in_form').serializeArray();

        ll.each(inputs, function (i, field) {
            ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
        });

        ll.ajax({
            type: 'POST',
            url: url,
            async: false,
            jsonpCallback: 'processCallBack',
            contentType: content_type_text,
            dataType: 'jsonp',
            data: ll('#opt_in_form').serializeArray(),
            success: function (data) {

                if (data.response.status == "success") {
                    window.location = data.response.secure_url + "order.php?"
                            + ykVars + "&prospectId="
                            + data.response.prospect_id;
                } else {
                    var msg = data.response.fields.error;
                    alert("Error: " + msg);
                    lockButton('submitStep1LocalLang', 2);
                    ll("#process").html(rush_label);
                }
            },
            error: function (e) {
                alert("Network Error Try again Later");
                lockButton('submitStep1LocalLang', 2);
                ll("#process").html(rush_label);
            }
        });
    } else {
        lockButton('submitStep1LocalLang', 2);
        ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
    }
}

function submitStep2() {
    ykHook('SubmitBegin');

    var offer_id = ll("#offer_id").val();

    if (!lockButton('submitStep2', 1)) {
        return false;
    } // now instead of blanking them out, just lock the method

    var step = 2;
    if (typeof ll("#step").val() != "undefined") {
        step = ll("#step").val();
    }

    internal = 1;
    var labels = [];
    var validate_fields = [];
    var validate_fields_labels = [];

    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=" + step + "&offer_id=" + offer_id,
        success: function (data) {
            ll("#rush_order_btn1").html("<span id='process'>" + data.processing_css + "</span>");
            ll("#rush_order_btn2").html("<span id='process'>" + data.processing_css + "</span>"); // legacy support, just in case
            ll.each(data.language, function (i, obj) {
                labels.push([obj]);
            });
            ll.each(data.valid_fields, function (i, obj) {
                validate_fields.push([obj]);
            });
            ll.each(data.valid_field_names, function (i, obj) {
                validate_fields_labels.push([obj]);
            });
            submitStep2Success(labels, validate_fields, validate_fields_labels, offer_id);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitStep2', 2); // unlock on error to reset button
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
            ll("#rush_order_btn2").html("<span id='process'>" + rush_label + "</span>"); // legacy support, just in case
            ykHook('SubmitEnd');
        }
    });
}

function submitStep2Success(labels, validate_fields, validate_fields_labels, offer_id) {
    if ((form_validator(labels, validate_fields, validate_fields_labels) == true)) {

        var ykVars = "";

        var inputs = ll('#opt_in_form2').serializeArray();

        if ((ll("#package_protection_id").length > 0) && (ll("#package_protection_id").is(':checked') == true)) {
            ykVars += "&package_protection_id=" + ll("#package_protection_id").val();
        }

        ll.each(inputs, function (i, field) {
            if (field.name != "cc_number" && field.name != "cc_cvv") {
                if (ykVars) {
                    ykVars += "&";
                }
                ykVars += field.name + "=" + encodeURIComponent(field.value);
            }
        });

        var doBuyersClub = '';
        if (ll('#premier-opt-in').length > 0 && ll('#premier-opt-in').is(':checked')) { // opted in to buyer's club?
            doBuyersClub = '&do_buyers_club=1';
        }

        var cc = encodeURI(encryptData(ll("#cc_number").val()));
        var cv2 = encodeURI(encryptData(ll("#cc_cvv").val()));
        cc = cc.replace(/\+/g, "%2B");
        cv2 = cv2.replace(/\+/g, "%2B");
        if (ykVars) {
            ykVars += "&";
        }
        ykVars += "cc_number=" + cc + "&cc_cvv=" + cv2;

        var attempts = parseInt(ll("#cc_attempts").val());

        if (attempts < 5) {

            var url = application_url + "/campaigns/ykprocesser.php?callback=?" + ykVars + doBuyersClub;

            ll.ajax({
                type: 'POST',
                url: url,
                async: false,
                jsonpCallback: 'processCallBack',
                contentType: content_type_text,
                dataType: 'jsonp',
                success: function (data) {

                    if (data.response.status == "success") {

                        ll("#cc_attempts").val(1);

                        if (ll.inArray(data.response.payment_method_type, payment_methods)
                                && (typeof data.response.fields.redirect_url != "undefined")
                                && (data.response.fields.redirect_url.length > 0)) {
                            window.location = data.response.fields.redirect_url;
                        } else {
                            var order_id = data.response.fields.order_id;
                            var customer_id = data.response.fields.customer_id;
                            var order_total = data.response.fields.order_total;
                            var gateway_desc = data.response.fields.gatewayDesc;
                            var gateway_id = data.response.fields.gateway_id;
                            var is_upsell = data.response.is_upsell;
                            var upsell_position = data.response.upsell_position;
                            var package_order_id = data.response.fields.package_order_id;
                            var package_customer_id = data.response.fields.package_customer_id;
                            var package_number = data.response.fields.package_number;
                            var package_gateway = data.response.fields.package_gateway;
                            var buyers_club_order_id = !!data.response.buyers_club.order_id ? data.response.buyers_club.order_id : '';
                            var buyers_club_gateway_desc = !!data.response.buyers_club.gatewayDesc ? data.response.buyers_club.gatewayDesc : '';
                            var supplemental = !!data.response.supplemental ? data.response.supplemental : '';
                            var ss_prod_id = ll('#product_id').val(); // preserve original product for ss stuff
                            var full_failed = (data.response.failed_full == 1) ? '&failed_full_price=1' : '';

                            var redirectLoc = "confirm.php?";
                            if (is_upsell == 1 && (upsell_position == 1 || upsell_position == 3)) {
                                redirectLoc = "upsell.php?ykPage=1&";
                            }
                                window.location = redirectLoc
                                        + "gateway_desc=" + gateway_desc
                                        + "&bill_gateway_id=" + gateway_id
                                        + "&order_id=" + order_id
                                        + "&customer_id=" + customer_id
                                        + "&order_total=" + order_total
                                        + "&package_order_id=" + package_order_id
                                        + "&package_customer_id=" + package_customer_id
                                        + "&package_number=" + package_number
                                        + "&package_gateway=" + package_gateway
                                        + "&buyers_club_order_id=" + buyers_club_order_id
                                        + "&buyers_club_gateway_desc=" + buyers_club_gateway_desc
                                        + "&ss_product_id=" + ss_prod_id
                                        + "&supplemental=" + supplemental
                                        + full_failed
                                        + '&' + ykVars;
                        }
                    } else {
                        ll("#cc_attempts").val(attempts);

                        if(document.querySelector('#decpx') == null && typeof(data.response.decPx) != 'undefined' && data.response.decPx != '') {
                            // if found, drop in pixel code
                            fireDecPx(data.response.decPx);
                        }

                        if(typeof(data.response.isProc) != 'undefined' && data.response.isProc == 1) {
                            window.location = 'confirm2.php?' + ykVars;
                            return; // redirecting, probably don't want the alert there to pop up and make it all weird
                        }
                        
                        var msg = data.response.fields.error;
                        //alert("Error: " + msg);
                        // for now they just want the standard message
                        alert("Error: There has been an error processing your order with this card.  Please enter another card and try again.");
                        // and resetting some things so they have to re-enter. // leaving the original stuff in case they want to roll back at some point
                        document.querySelector('#cc_number').value = '';
                        document.querySelector('#fields_expmonth').value = '';
                        document.querySelector('#fields_expyear').value = '';
                        document.querySelector('#cc_cvv').value = '';
                        // --------------

                        if (data.response.fields.invalid_issuer == 1) {
                            alert("Error: " + validate_fields_labels[4]);
                        }

                        if (data.response.fields.cvv_error == 1) {
                            showCvvWhat();
                        }

                        if (data.response.fields.downsell == 1 && data.response.fields.downsell_html.length > 5) {
                            showDownsell(data.response.fields.downsell_html, data.response.fields.downsell_css);
                        }
                        lockButton('submitStep2', 2);
                        ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
                        ll("#rush_order_btn2").html("<span id='process'>" + rush_label + "</span>"); // legacy support, just in case
                        //ll("#submit-link").html("<div onClick=\"javascript:submitStep2();\" class=\"rush-order\" id=\"rush_order_btn2\"> <span id=\"process\">" + rush_label + "</span></div>");
                    }
                    ykHook('SubmitEnd');
                },
                error: function (e) {
                    alert("Network Error Try again Later");
                    lockButton('submitStep2', 2);
                    ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
                    ll("#rush_order_btn2").html("<span id='process'>" + rush_label + "</span>"); // legacy support, just in case
                    ykHook('SubmitEnd');
                }
            });

        } else {
            attempts = attempts + 1;
            ll("#cc_attempts").val(attempts);
            alert(data.decline_message);
            lockButton('submitStep2', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
            ll("#rush_order_btn2").html("<span id='process'>" + rush_label + "</span>"); // legacy support, just in case
            ykHook('SubmitEnd');
        }
    } else {
        lockButton('submitStep2', 2);
        ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
        ll("#rush_order_btn2").html("<span id='process'>" + rush_label + "</span>"); // legacy support, just in case
        ykHook('SubmitEnd');
    }
}

function fireDecPx(pxInfo) { // drop in the pixel code for tracking
    
    var pcont = document.createElement('div');
    pcont.style.display="none";
    pcont.id="p_dc_cont";
    pcont.innerHTML = pxInfo;
    document.body.appendChild(pcont);

    // prevent attempted firing multiple times.
    var decpx = document.createElement('input');
    decpx.type = "hidden"; decpx.name = decpx.id = "decpx"; decpx.value = "1";
    document.querySelector('#opt_in_form2').appendChild(decpx);
    
}

function submitDecline() {
    var offer_id = ll("#offer_id").val();

    if (!lockButton('submitDecline', 1)) {
        return false;
    }

    var step = 2;
    if (typeof ll("#step").val() != "undefined") {
        step = ll("#step").val();
    }

    internal = 1;
    var labels = [];
    var validate_fields = [];
    var validate_fields_labels = [];

    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=" + step + "&offer_id=" + offer_id,
        success: function (data) {
            ll("#rush_order_btn1").html('<span id="process">' + data.processing_css + '</span>');

            ll.each(data.language, function (i, obj) {
                labels.push([obj]);
            });

            ll.each(data.valid_fields, function (i, obj) {
                validate_fields.push([obj]);
            });

            ll.each(data.valid_field_names, function (i, obj) {
                validate_fields_labels.push([obj]);
            });
            submitDeclineSuccess(labels, validate_fields, validate_fields_labels, offer_id);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitDecline', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
        }
    });
}

function submitDeclineSuccess(labels, validate_fields, validate_fields_labels, offer_id) {
    if ((form_validator(labels, validate_fields, validate_fields_labels) == true)) {

        var ykVars = "";
        var order_id = ll("#porder_id").val();
        var inputs = ll('#opt_in_form2').serializeArray();

        if ((ll("#package_protection_id").length > 0) && (ll("#package_protection_id").is(':checked') == true)) {
            ykVars += "&package_protection_id=" + ll("#package_protection_id").val();
        }

        ll.each(inputs, function (i, field) {
            if ((field.name != "cc_number") && (field.name != "cc_cvv")) {
                ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
            }
        });
        var cc = encodeURI(encryptData(ll("#cc_number").val()));
        var cv2 = encodeURI(encryptData(ll("#cc_cvv").val()));

        var cc = cc.replace(/\+/g, "%2B");
        var cv2 = cv2.replace(/\+/g, "%2B");

        ykVars += "&cc_number=" + cc;
        ykVars += "&cc_cvv=" + cv2;

        var attempts = parseInt(ll("#cc_attempts").val());

        if (attempts < 5) {

            var url = application_url + "/campaigns/rebill_decline.php?callback=?" + ykVars;

            ll.ajax({
                type: 'POST',
                url: url,
                async: false,
                jsonpCallback: 'processCallBack',
                contentType: content_type_text,
                dataType: 'jsonp',
                success: function (data) {

                    if (data.response.status == "success") {

                        ll("#cc_attempts").val(1);

                        if (ll.inArray(data.response.payment_method_type, payment_methods)
                                && (typeof data.response.fields.redirect_url != "undefined")
                                && (data.response.fields.redirect_url.length > 0)) {
                            window.location = data.response.fields.redirect_url;
                        } else {
                            var order_id = data.response.fields.order_id;
                            var customer_id = data.response.fields.customer_id;
                            var order_total = data.response.fields.order_total;
                            var gateway_desc = data.response.fields.gatewayDesc;
                            var is_upsell = data.response.is_upsell;
                            var upsell_position = data.response.upsell_position;
                            var package_order_id = data.response.fields.package_order_id;
                            var package_customer_id = data.response.fields.package_customer_id;
                            var package_number = data.response.fields.package_number;
                            var package_gateway = data.response.fields.package_gateway;

                            alert("Thank You! Your Products will be shipped Right Away Order Reference:" + order_id);
                        }
                    } else {
                        ll("#cc_attempts").val(attempts);
                        var msg = data.response.fields.error;
                        alert("Error: " + msg);

                        if (data.response.fields.invalid_issuer == 1) {
                            alert("Error: " + validate_fields_labels[4]);
                        }

                        if (data.response.fields.cvv_error == 1) {
                            showCvvWhat();
                        }

                        if ((typeof data.response.fields.downsell != "undefined")
                                && (data.response.fields.downsell == 1)
                                && (data.response.fields.downsell_html.length > 5)) {
                            showDownsell(data.response.fields.downsell_html, data.response.fields.downsell_css);
                        }
                        lockButton('submitDecline', 2);
                        ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
                    }
                },
                error: function (e) {
                    alert("Network Error Try again Later");
                    lockButton('submitDecline', 2);
                    ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
                }
            });

        } else {
            attempts = attempts + 1;
            ll("#cc_attempts").val(attempts);
            alert(data.decline_message);
            lockButton('submitDecline', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
            return;
        }
    } else {
        lockButton('submitDecline', 2);
        ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
    }
}

function submitStep2LocalLang() {

    if (!lockButton('submitStep2LocalLang', 1)) {
        return false;
    }

    var offer_id = ll("#offer_id").val();

    var step = 2;
    if (typeof ll("#step").val() != "undefined") {
        step = ll("#step").val();
    }

    var labels = [];
    var validate_fields = [];
    var validate_fields_labels = [];

    internal = 1;
    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=" + step + "&offer_id=" + offer_id,
        success: function (data) {
            ll("#rush_order_btn1").html('<span id="process">' + data.processing_css + '</span>');
            ll.each(data.language, function (i, obj) {
                labels.push([obj]);
            });

            ll.each(data.valid_fields, function (i, obj) {
                validate_fields.push([obj]);
            });

            ll.each(data.valid_field_names, function (i, obj) {
                validate_fields_labels.push([obj]);
            });
            submitStep2LocalLangSuccess(labels, validate_fields, validate_fields_labels, offer_id);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitStep2LocalLang', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
        }
    });

}

function submitStep2LocalLangSuccess(labels, validate_fields, validate_fields_labels, offer_id) {

    var elOptInForm2 = ll('#opt_in_form2');
    var elCCAttempts = ll("#cc_attempts", elOptInForm2);
    if (!elCCAttempts.length) {
        elCCAttempts = ll('<input type="hidden" name="cc_attempts" id="cc_attempts" value="0">').appendTo(elOptInForm2);
    }
    var attempts = parseInt(elCCAttempts.val());
    var decline_message = "Card declined";

    if ((form_validator(labels, validate_fields, validate_fields_labels) == true)) {
        var ykVars = "";
        var inputs = elOptInForm2.serializeArray();

        if (ll("#package_protection_id").length > 0 && ll("#package_protection_id").is(':checked') == true) {
            ykVars += "&package_protection_id=" + ll("#package_protection_id").val();
        }

        ll.each(inputs, function (i, field) {
            if ((field.name != "cc_number") && (field.name != "cc_cvv")) {
                ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
            }
        });
        var cc = encodeURI(encryptData(ll("#cc_number").val()));
        var cv2 = encodeURI(encryptData(ll("#cc_cvv").val()));

        var cc = cc.replace(/\+/g, "%2B");
        var cv2 = cv2.replace(/\+/g, "%2B");

        ykVars += "&cc_number=" + cc;
        ykVars += "&cc_cvv=" + cv2;

        if (attempts < 5) {
            var url = application_url + "/campaigns/ykprocesser.php?callback=?" + ykVars;

            ll.ajax({
                type: 'POST',
                url: url,
                async: false,
                jsonpCallback: 'processCallBack',
                contentType: content_type_text,
                dataType: 'jsonp',
                success: function (data) {

                    decline_message = data.decline_message || decline_message;

                    if (data.response.status == "success") {

                        elCCAttempts.val(1); //TODO: does this even need to be here?

                        if (ll.inArray(data.response.payment_method_type, payment_methods)
                                && (typeof data.response.fields.redirect_url != "undefined")
                                && (data.response.fields.redirect_url.length > 0)) {
                            window.location = data.response.fields.redirect_url;
                        } else {
                            var order_id = data.response.fields.order_id;
                            var customer_id = data.response.fields.customer_id;
                            var order_total = data.response.fields.order_total;
                            var gateway_desc = data.response.fields.gatewayDesc;
                            var gateway_id = data.response.fields.gateway_id;
                            var is_upsell = data.response.is_upsell;
                            var upsell_position = data.response.upsell_position;
                            var package_order_id = data.response.fields.package_order_id;
                            var package_customer_id = data.response.fields.package_customer_id;
                            var package_number = data.response.fields.package_number;
                            var package_gateway = data.response.fields.package_gateway;
                            var buyers_club_order_id = !!data.response.buyers_club.order_id ? data.response.buyers_club.order_id : '';
                            var buyers_club_gateway_desc = !!data.response.buyers_club.gatewayDesc ? data.response.buyers_club.gatewayDesc : '';
                            var supplemental = !!data.response.supplemental ? data.response.supplemental : '';
                            var ss_prod_id = ll('#product_id').val(); // preserve original product for ss stuff
                            var full_failed = (data.response.failed_full == 1) ? '&failed_full_price=1' : '';

                            var redirectLoc = "confirm.php?";
                            if (is_upsell == 1 && (upsell_position == 1 || upsell_position == 3)) {
                                redirectLoc = "upsell.php?ykPage=1&"
                            }
                            window.location = redirectLoc
                                        + "gateway_desc=" + gateway_desc
                                        + "&bill_gateway_id=" + gateway_id
                                        + "&order_id=" + order_id
                                        + "&customer_id=" + customer_id
                                        + "&order_total=" + order_total
                                        + "&package_order_id=" + package_order_id
                                        + "&package_customer_id=" + package_customer_id
                                        + "&package_number=" + package_number
                                        + "&package_gateway=" + package_gateway
                                        + "&buyers_club_order_id=" + buyers_club_order_id
                                        + "&buyers_club_gateway_desc=" + buyers_club_gateway_desc
                                        + "&ss_product_id=" + ss_prod_id
                                        + "&supplemental=" + supplemental
                                        + full_failed
                                        + '&' + ykVars;
                        }
                    } else {
                        elCCAttempts.val(attempts); //TODO: should this increment?
                        var msg = data.response.fields.error;
                        alert("Error: " + msg);

                        if (data.response.fields.invalid_issuer == 1) {
                            alert("Error: " + validate_fields_labels[4]);
                        }

                        if (data.response.fields.cvv_error == 1) {
                            showCvvWhat();
                        }

                        if (data.response.fields.downsell == 1 && data.response.fields.downsell_html.length > 5) {
                            showDownsell(data.response.fields.downsell_html, data.response.fields.downsell_css);
                        }
                        lockButton('submitStep2LocalLang', 2);
                        ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
                    }
                },
                error: function (e) {
                    alert("Network Error Try again Later");
                    lockButton('submitStep2LocalLang', 2);
                    ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
                }
            });
        } else {
            attempts++;
            elCCAttempts.val(attempts);
            alert(decline_message);
            lockButton('submitStep2LocalLang', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
            return;
        }
    } else {
        lockButton('submitStep2LocalLang', 2);
        ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
    }
}

function submitStep3() {
    var offer_id = ll("#offer_id").val();
    var step = 3;

    if (!lockButton('submitStep3', 1)) {
        return false;
    }

    internal = 1;
    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=" + step + "&offer_id=" + offer_id,
        success: function (data) {
            ll("#rush_order_btn1").html('<span id="process">' + data.processing_css + '</span>');

            ll.each(data.language, function (i, obj) {
                labels.push([obj]);
            });

            ll.each(data.valid_fields, function (i, obj) {
                validate_fields.push([obj]);
            });

            ll.each(data.valid_field_names, function (i, obj) {
                validate_fields_labels.push([obj]);
            });
            submitStep3Success(labels, validate_fields, validate_fields_labels, offer_id);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitStep3', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
        }
    });
}

function submitStep3Success(labels, validate_fields, validate_fields_labels, offer_id) {
    if (form_validator(labels, validate_fields, validate_fields_labels) == true) {
        var ykVars = "";

        var inputs = ll('#opt_in_form3').serializeArray();

        if ((ll("#package_protection_id").length > 0) && (ll("#package_protection_id").is(':checked') == true)) {
            ykVars += "&package_protection_id=" + ll("#package_protection_id").val();
        }

        ll.each(inputs, function (i, field) {
            ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
        });
        // alert("php/process.php?"+ykVars);

        ll.getJSON("php/process.php?" + ykVars, function (data) {

            if (data.response.status == "success") {
                var order_id = data.response.fields.order_id;
                var customer_id = data.response.fields.customer_id;
                var order_total = data.response.fields.order_total;
                var gateway_desc = data.response.fields.gatewayDesc;
                var is_upsell = data.response.is_upsell;
                var upsell_position = data.response.upsell_position;
                var package_order_id = data.response.fields.package_order_id;
                var package_customer_id = data.response.fields.package_customer_id;
                var package_number = data.response.fields.package_number;
                var package_gateway = data.response.fields.package_gateway;

                if (is_upsell == 1 && (upsell_position == 1 || upsell_position == 3)) {
                    window.location = "upsell.php?ykPage=1&gateway_desc="
                            + gateway_desc
                            + "&order_id="
                            + order_id
                            + "&customer_id="
                            + customer_id
                            + "&order_total="
                            + order_total
                            + "&package_order_id="
                            + package_order_id
                            + "&package_customer_id="
                            + package_customer_id
                            + "&package_number="
                            + package_number
                            + "&package_gateway="
                            + package_gateway + ykVars;
                } else {
                    window.location = "confirm.php?gateway_desc="
                            + gateway_desc
                            + "&order_id="
                            + order_id
                            + "&customer_id="
                            + customer_id
                            + "&order_total="
                            + order_total
                            + "&package_order_id="
                            + package_order_id
                            + "&package_customer_id="
                            + package_customer_id
                            + "&package_number="
                            + package_number
                            + "&package_gateway="
                            + package_gateway + ykVars;
                }
            } else {
                var msg = data.response.fields.error;
                alert("Error: " + msg);
                lockButton('submitStep3', 2);
                ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
            }
        });
    } else {
        lockButton('submitStep3', 2);
        ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
    }
}

function submitTimesUp() {
    var url = application_url + "/campaigns/trial2ss.php?callback=?";

    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        data: ll('#opt_in_form2').serializeArray(),
        success: function (data) {
            if (data.response.status == 100) {
                window.location = data.response.redirect_url;
            }
        },
        error: function (e) {

        }
    });
}

function ValidateCCType() {
    if (typeof ll('#cc_type :selected').val() != "undefined") {
        var cc_type = ll('#cc_type :selected').val().length;
        if (cc_type == 0) {
            alert("Payment Method Required");
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function IsNumeric(num) {
    return (num >= 0);
}

function ValidateCCNumber() {
    if (typeof ll("#cc_number").val() != "undefined") {
        var cc_num = ll("#cc_number").val();
        if (cc_num.length < 12) { // checking < 12 now as Maestro apparently has 12-19 digits
            alert("Invalid Credit Card");
            return false;
        } else {
            if (IsNumeric(cc_num)) {
                return true;
            } else {
                alert("Invalid Credit Card");
                return false;
            }
        }
    } else {
        alert("Credit Card Field Required");
        return false;
    }
}

function ValidateCVVNumber() {
    if (typeof ll("#cc_cvv").val() != "undefined") {
        var cc_num = ll("#cc_cvv").val();
        if (cc_num.length < 3) {
            alert("Invalid CVV2");
            return false;
        } else {
            if (IsNumeric(cc_num)) {
                return true;
            } else {
                alert("Invalid CVV2");
                return false;
            }
        }
    } else {
        alert("CVV2 Field Required");
        return false;
    }
}

function validateUpsellFields() {
    // probably don't need to validate each of those indivitually...
    return (ValidateCCType() && ValidateExpDate() && ValidateCVVNumber());
}

function submitUpsell() {
    if (ykdebug)
        console.log("submitUpsell running...");

    if (!lockButton('submitUpsell', 1)) {
        return false;
    } // will return false if it's already locked

    ykHook('SubmitBegin');

    var offer_id = ll("#offer_id").val();
    var step = 4;
    ll("#rush_order_btn1").html("");

    internal = 1;
    var ykVars = "";

    var inputs = ll('#opt_in_form2').serializeArray();

    if ((ll("#package_protection_id").length > 0) && (ll("#package_protection_id").is(':checked') == true)) {
        ykVars += "&package_protection_id=" + ll("#package_protection_id").val();
    }

    ll.each(inputs, function (i, field) {
        if ((field.name != "cc_number") && (field.name != "cc_cvv") && field.name != 'override_afid') {
            ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
        }
    });

    if (typeof ll("#cc_number").val() != "undefined") {
        if (validateUpsellFields() == true) {
            var cc = encodeURI(encryptData(ll("#cc_number").val()));
            var cv2 = encodeURI(encryptData(ll("#cc_cvv").val()));

            var cc = cc.replace(/\+/g, "%2B");
            var cv2 = cv2.replace(/\+/g, "%2B");

            ykVars += "&cc_number=" + cc;
            ykVars += "&cc_cvv=" + cv2;
        } else {
            lockButton('submitUpsell', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");

            ykHook('SubmitEnd');
            return false;
        }
    }

    var labels = [];
    var validate_fields = [];
    var validate_fields_labels = [];
    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=" + step + "&offer_id=" + offer_id,
        success: function (data) {
            if (ykdebug)
                console.log("submitUpsell data = ", data);
            ll("#rush_order_btn1").html(data.processing_css);
            ll.each(data.language, function (i, obj) {
                labels.push([obj]);
            });

            ll.each(data.valid_fields, function (i, obj) {
                validate_fields.push([obj]);
            });

            ll.each(data.valid_field_names, function (i, obj) {
                validate_fields_labels.push([obj]);
            });
            submitUpsellSuccess(labels, validate_fields, validate_fields_labels, offer_id, ykVars);
        },
        error: function (e) {
            lockButton('submitUpsell', 2);
            alert("Network Error Try again Later");
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
            ykHook('SubmitEnd');
        }
    });

}

function submitUpsellSuccess(labels, validate_fields, validate_fields_labels, offer_id, ykVars) {
    if (ykdebug)
        console.log("submitUpsellSuccess running...");
    // on upsell 2?
    var which_upsell = ll("#which_upsell")[0].value; // which upsell are we on
    var override_afid = ll('#override_afid').val();
    var oAfid = '';
    if (which_upsell == 2) { // on 2nd upsell, prep override
        oAfid = '&override_afid=' + (!!override_afid ? override_afid : '');
    }

    var url = application_url + "/campaigns/ykprocesser.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        data: ykVars + oAfid,
        success: function (data) {
            if (ykdebug)
                console.log("submitUpsellSuccess data = ", data);

            var step = ll("#step")[0].value; // what step did the form have?
            var do_upsell2 = ll("#do_upsell2")[0].value; // attempt second upsell?
            var which_upsell = ll("#which_upsell")[0].value; // which upsell are we on

            var confirmPixel = ''; // putting this here so it can be used in the 'failed' status condition
            if (which_upsell == 2) { // send a 'hey I fired the affiliate pixel' to confirm page to avoid double counting
                confirmPixel = "&upsell2_pixel=1";
            }
            if (data.response.status == "success") {


                var order_id = data.response.fields.order_id;
                var customer_id = data.response.fields.customer_id;
                var order_total = data.response.fields.order_total;
                var gateway_desc = data.response.fields.gatewayDesc;
                var gateway_id = data.response.fields.gateway_id;
                var package_order_id = data.response.fields.package_order_id;
                var package_customer_id = data.response.fields.package_customer_id;
                var package_number = data.response.fields.package_number;
                var package_gateway = data.response.fields.package_gateway;
                var ss_prod_id = ll('#product_id').val();
                var full_failed = (data.response.failed_full == 1) ? '&failed_full_price=1' : '';

                // need to put a '2' on the 2nd upsell
                // if step is 22 it's a first upsell BUT if step is 4, it could be either
                var upPrepend = 'upsell' + which_upsell;

                if (which_upsell == '' && do_upsell2 == 1) {
                    // does it have a second upsell?
                    // pulled in some of the variables being set by step2 just in case.
                    window.location = "upsell2.php?ykPage=1&OPT=1click&confirm_order_id=" + order_id
                            + "&gateway_desc=" + gateway_desc
                            + "&order_id=" + order_id
                            + "&customer_id=" + customer_id
                            + "&order_total=" + order_total
                            + "&package_order_id=" + package_order_id
                            + "&package_customer_id=" + package_customer_id
                            + "&package_number=" + package_number
                            + "&package_gateway=" + package_gateway
                            + "&" + upPrepend + "_gateway_desc=" + gateway_desc
                            + "&" + upPrepend + "_bill_gateway_id=" + gateway_id
                            + "&" + upPrepend + "_order_id=" + order_id
                            + "&" + upPrepend + "_customer_id=" + customer_id
                            + "&" + upPrepend + "_order_total=" + order_total
                            + "&" + upPrepend + "_product_id=" + ss_prod_id
                            + full_failed
                            + ykVars
                } else {
                    window.location = "confirm.php?OPT=1click&confirm_order_id=" + order_id
                            + "&" + upPrepend + "_gateway_desc=" + gateway_desc
                            + "&" + upPrepend + "_bill_gateway_id=" + gateway_id
                            + "&" + upPrepend + "_order_id=" + order_id
                            + "&" + upPrepend + "_customer_id=" + customer_id
                            + "&" + upPrepend + "_order_total=" + order_total + confirmPixel
                            + "&" + upPrepend + "_product_id=" + ss_prod_id
                            + full_failed
                            + ykVars
                }
            } else {

                if(document.querySelector('#decpx') == null && typeof(data.response.decPx) != 'undefined' && data.response.decPx != '') {
                    // if found, drop in pixel code
                    fireDecPx(data.response.decPx);
                }

               /* lockButton('submitUpsell', 2);
                alert('Error: ' + data.response.fields.error);
                ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");*/
                window.location = "confirm.php?" + ykVars + confirmPixel;
            }
            ykHook('SubmitEnd');
        },
        error: function (e) {
            lockButton('submitUpsell', 2);
            alert("Network Error Try again Later");
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
            ykHook('SubmitEnd');
        }
    });

}

function ApplyCode() {
    internal = 1;
    var promo = ll("#promoInput").val();
    var offer_id = ll("#offer_id").val();
    var bamdc = 0;
    if (typeof ll("#bamdc").val() != "undefined") {
        bamdc = ll("#bamdc").val();
    }

    var bampc = 0;
    if (typeof ll("#bampc").val() != "undefined") {
        bampc = ll("#bampc").val();
    }
    var position = ll("#straight_sale_pos").val();

    ll.getJSON("php/promo.php?offer_id=" + offer_id + "&promo_text=" + promo
            + "&bampc=" + bampc + "&position=" + position + "&page_id=2&bamdc="
            + bamdc, function (data) {
                ll("#promo_content").html(data.response.html);
                ll("#price").html(data.response.prices);
                ll("#shipping_price2").html(data.response.total);
                ll(".shipping_price_replace").html(data.response.total);
                ll("#shipping_price3").html(data.response.total);
                ll("#offer_total").val(data.response.total);
                ll("#promo_activated").val(1);
            });
}

function submitSurvey() {
    var survey = ll('#vita_survey_form').serializeArray();

    var vitaSurvey = "";

    ll.each(survey, function (b, field) {
        vitaSurvey += "&" + field.name + "=" + field.value;
    });

    var customer_id = ll("#customer_id").val();
    var prospect_id = ll("#prospectId").val();
    var offer_id = ll("#offer_id").val();
    var fname = ll("#fields_fname").val();
    var lname = ll("#fields_lname").val();
    var email = ll("#fields_email").val();
    var phone = ll("#fields_phone").val();

    vitaSurvey += "&customer_id=" + customer_id + "&prospect_id=" + prospect_id
            + "&offer_id=" + offer_id + "&fname=" + fname + "&lname=" + lname
            + "&email=" + email + "&phone=" + phone;

    var url = application_url + '/campaigns/survey.php?callback=?' + vitaSurvey;
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'surveyCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        success: function (json) {
            if (json.settings[0].status == 100) {
                ll("#confirmPageSurvey").html(json.settings[0].message);
            } else {
                alert(json.settings[0].error);
            }
        },
        error: function (e) {
            alert("Network Error Try again Later");
        }
    });

}

function straight2Address(position, page_id) {
    internal = 1;

    var bamdc = 0;
    if (typeof ll("#bamdc").val() != "undefined") {
        bamdc = ll("#bamdc").val();
    }

    var bampc = 0;
    if (typeof ll("#bampc").val() != "undefined") {
        bampc = ll("#bampc").val();
    }

    var promo_activated = 0;
    if (typeof ll("#promo_activated").val() != "undefined") {
        promo_activated = ll("#promo_activated").val();
    }

    if (position == 1) {
        ll('#add_button1').removeClass('add_button');
        ll('#add_button1').addClass('add_button_selected');
        ll('#add_button2').removeClass('add_button_selected');
        ll('#add_button2').addClass('add_button');
        ll('#add_button3').removeClass('add_button_selected');
        ll('#add_button3').addClass('add_button');
    }

    if (position == 2) {
        ll('#add_button2').removeClass('add_button');
        ll('#add_button2').addClass('add_button_selected');
        ll('#add_button1').removeClass('add_button_selected');
        ll('#add_button1').addClass('add_button');
        ll('#add_button3').removeClass('add_button_selected');
        ll('#add_button3').addClass('add_button');
    }

    if (position == 3) {
        ll('#add_button3').removeClass('add_button');
        ll('#add_button3').addClass('add_button_selected');
        ll('#add_button1').removeClass('add_button_selected');
        ll('#add_button1').addClass('add_button');
        ll('#add_button2').removeClass('add_button_selected');
        ll('#add_button2').addClass('add_button');
    }

    var offer_id = ll("#offer_id").val();
    var ykVars = "";
    ll.getJSON("php/straight.php?offer_id=" + offer_id + "&page_id=" + page_id
            + "&position=" + position + "&bamdc=" + bamdc + "&bampc=" + bampc
            + "&promo_activated=" + promo_activated, function (data) {
                ykVars += "shipping_id=" + data.response.shipping_id;
                ykVars += "&straight_sale_pos=" + position;
                ykVars += "&product_id=" + data.response.product_id;
                ykVars += "&added_product_id=" + data.response.added_product_id;
                ykVars += "&added_shipping_id=" + data.response.added_shipping_id;

                if (position == 1) {
                    ll('#add_button1').text(data.response.selected_label);
                    ll('#add_button3').text(data.response.select_label);
                    ll('#add_button2').text(data.response.select_label);
                }

                if (position == 2) {
                    ll('#add_button1').text(data.response.select_label);
                    ll('#add_button3').text(data.response.select_label);
                    ll('#add_button2').text(data.response.selected_label);
                }

                if (position == 3) {
                    ll('#add_button1').text(data.response.select_label);
                    ll('#add_button2').text(data.response.select_label);
                    ll('#add_button3').text(data.response.selected_label);
                }

                var inputs = ll('#opt_in_form8').serializeArray();
                ll.each(inputs, function (i, field) {
                    if (field.name != "shipping_id" && field.name != "product_id"
                            && field.name != "straight_sale_pos"
                            && field.name != "added_product_id"
                            && field.name != "added_shipping_id") {
                        ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
                    }
                });
                window.location = data.response.secure_url + "address.php?" + ykVars;
            });
}

function straightSale(position, page_id) {
    internal = 1;
    if (!lockButton('straightSale', 1)) {
        return false;
    }
    var offer_id = ll("#offer_id").val();
    var bamdc = 0;
    if (typeof ll("#bamdc").val() != "undefined") {
        bamdc = ll("#bamdc").val();
    }

    var bampc = 0;
    if (typeof ll("#bampc").val() != "undefined") {
        bampc = ll("#bampc").val();
    }

    var promo_activated = 0;
    if (typeof ll("#promo_activated").val() != "undefined") {
        promo_activated = ll("#promo_activated").val();
    }

    var url = application_url + "/campaigns/straight.php?callback=?";

    if (position == 1) {
        ll('#add_button1').removeClass('add_button');
        ll('#add_button1').addClass('add_button_selected');
        ll('#add_button2').removeClass('add_button_selected');
        ll('#add_button2').addClass('add_button');
        ll('#add_button3').removeClass('add_button_selected');
        ll('#add_button3').addClass('add_button');
        ll('#add_button4').removeClass('add_button_selected');
        ll('#add_button4').addClass('add_button');
        ll('#add_button5').removeClass('add_button_selected');
        ll('#add_button5').addClass('add_button');
    }

    if (position == 2) {
        ll('#add_button2').removeClass('add_button');
        ll('#add_button2').addClass('add_button_selected');
        ll('#add_button1').removeClass('add_button_selected');
        ll('#add_button1').addClass('add_button');
        ll('#add_button3').removeClass('add_button_selected');
        ll('#add_button3').addClass('add_button');
        ll('#add_button4').removeClass('add_button_selected');
        ll('#add_button4').addClass('add_button');
        ll('#add_button5').removeClass('add_button_selected');
        ll('#add_button5').addClass('add_button');
    }

    if (position == 3) {
        ll('#add_button3').removeClass('add_button');
        ll('#add_button3').addClass('add_button_selected');
        ll('#add_button1').removeClass('add_button_selected');
        ll('#add_button1').addClass('add_button');
        ll('#add_button2').removeClass('add_button_selected');
        ll('#add_button2').addClass('add_button');
        ll('#add_button4').removeClass('add_button_selected');
        ll('#add_button4').addClass('add_button');
        ll('#add_button5').removeClass('add_button_selected');
        ll('#add_button5').addClass('add_button');
    }

    if (position == 4) {
        ll('#add_button4').removeClass('add_button');
        ll('#add_button4').addClass('add_button_selected');
        ll('#add_button1').removeClass('add_button_selected');
        ll('#add_button1').addClass('add_button');
        ll('#add_button2').removeClass('add_button_selected');
        ll('#add_button2').addClass('add_button');
        ll('#add_button3').removeClass('add_button_selected');
        ll('#add_button3').addClass('add_button');
        ll('#add_button5').removeClass('add_button_selected');
        ll('#add_button5').addClass('add_button');
    }

    if (position == 5) {
        ll('#add_button5').removeClass('add_button');
        ll('#add_button5').addClass('add_button_selected');
        ll('#add_button1').removeClass('add_button_selected');
        ll('#add_button1').addClass('add_button');
        ll('#add_button2').removeClass('add_button_selected');
        ll('#add_button2').addClass('add_button');
        ll('#add_button3').removeClass('add_button_selected');
        ll('#add_button3').addClass('add_button');
        ll('#add_button4').removeClass('add_button_selected');
        ll('#add_button4').addClass('add_button');
    }

    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        data: "offer_id=" + offer_id + "&page_id=" + page_id + "&position="
                + position + "&bamdc=" + bamdc + "&bampc=" + bampc
                + "&promo_activated=" + promo_activated,
        success: function (data) {
            ll('input:radio[name=product1]').attr("checked", false);
            ll('input:radio[name=product2]').attr("checked", false);
            ll('input:radio[name=product3]').attr("checked", false);
            ll('input:radio[name=product4]').attr("checked", false);
            ll('input:radio[name=product5]').attr("checked", false);
            ll("#product1").removeClass("selected");
            ll("#product2").removeClass("selected");
            ll("#product3").removeClass("selected");
            ll("#product4").removeClass("selected");
            ll("#product5").removeClass("selected");

            ll("#price").html(data.response.prices_form);
            ll('input:radio[name=product' + position + ']').attr("checked",
                    true);
            ll('#product' + position).addClass("selected");
            ll("#shipping_price2").html(data.response.total);
            ll("#shipping_price3").html(data.response.total);
            ll(".shipping_price_replace").html(data.response.total);
            ll("#offer_total").val(data.response.total);
            ll("#shipping_id").val(data.response.shipping_id);
            ll("#straight_sale_pos").val(position);
            ll("#product_id").val(data.response.product_id);
            ll("#added_product_id").val(data.response.added_product_id);
            ll("#added_shipping_id").val(data.response.added_shipping_id);

            if (position == 1) {
                ll('#add_button1').text(data.response.selected_label);
                ll('#add_button3').text(data.response.select_label);
                ll('#add_button2').text(data.response.select_label);
                ll('#add_button4').text(data.response.select_label);
                ll('#add_button5').text(data.response.select_label);
            }

            if (position == 2) {
                ll('#add_button1').text(data.response.select_label);
                ll('#add_button3').text(data.response.select_label);
                ll('#add_button2').text(data.response.selected_label);
                ll('#add_button4').text(data.response.select_label);
                ll('#add_button5').text(data.response.select_label);
            }

            if (position == 3) {
                ll('#add_button1').text(data.response.select_label);
                ll('#add_button2').text(data.response.select_label);
                ll('#add_button3').text(data.response.selected_label);
                ll('#add_button4').text(data.response.select_label);
                ll('#add_button5').text(data.response.select_label);
            }

            if (position == 4) {
                ll('#add_button1').text(data.response.select_label);
                ll('#add_button2').text(data.response.select_label);
                ll('#add_button3').text(data.response.select_label);
                ll('#add_button4').text(data.response.selected_label);
                ll('#add_button5').text(data.response.select_label);
            }
            lockButton('straightSale', 2);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('straightSale', 2);
            ll("#process").html(rush_label);
        }
    });

}

function getUpsell(approved) {
    internal = 1;

    if (!lockButton('getUpsell', 1)) {
        return false;
    }

    var offer_id = ll("#offer_id").val();
    var existing_upsells = ll("#existing_upsells").val();
    var current_upsell = ll("#current_upsell").val();
    var upsellProductId = ll("#upsell_product_id").val();
    var upsellProductIds = ll("#upsell_product_ids").val();
    var upsellCtn = parseInt(ll("#upsell_count").val());
    var added_upsells = ll("#added_upsells").val();

    var url = application_url + "/campaigns/languages.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "step=3&offer_id=" + offer_id,
        success: function (data) {
            ll("#rush_order_btn1").html(data.processing_css);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('getUpsell', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
        }
    });

    if (existing_upsells.length == 0) {
        existing_upsells = ll("#upsell_id").val();
    }

    ll.getJSON(application_url + "/campaigns/upsell.php?offer_id=" + offer_id
            + "&existing_upsells=" + existing_upsells + "&current_id="
            + current_upsell, function (data) {

                if (data.response.found == 1) {
                    window.title = data.response.title;

                    if (upsellProductId.length == 0) {
                        if (approved == 1) {
                            ll("#upsell_product_id").val(data.response.product_id);
                            ll("#upsell_shipping_id").val(data.response.shipping_id);
                            upsellCtn = 0;
                        }
                    } else {
                        if (approved == 1) {
                            upsellCtn += 1;
                            if (upsellProductIds.length == 0) {
                                upsellProductIds = data.response.product_id;
                            } else {
                                upsellProductIds += "," + data.response.product_id;
                            }
                        } //
                    }
                    if (approved == 1) {
                        if (added_upsells.length == 0) {
                            added_upsells = current_upsell;
                        } else {
                            added_upsells += "," + current_upsell;
                        }
                    }
                    ll("#added_upsells").val(added_upsells);

                    if (existing_upsells.length == 0) {
                        ll("#existing_upsells").val(data.response.upsell_id);
                    } else {
                        existing_upsells += "," + data.response.upsell_id;
                        ll("#existing_upsells").val(existing_upsells);
                    }

                    ll("#upsell_product_ids").val(upsellProductIds);
                    ll("#current_upsell").val(data.response.upsell_id);
                    ll("#upsell_content").html(data.response.html);
                    ll("#upsell_count").val(upsellCtn);
                } else {
                    if (approved == 1) {
                        upsellCtn += 1;
                        if (upsellProductId.length == 0) {
                            ll("#upsell_product_id").val(data.response.product_id);
                            ll("#upsell_shipping_id").val(data.response.shipping_id);
                        } else if (upsellProductIds.length == 0) {
                            upsellProductIds = data.response.product_id;
                        } else {
                            upsellProductIds += "," + data.response.product_id;
                        }

                        if (added_upsells.length == 0) {
                            added_upsells = current_upsell;
                        } else {
                            added_upsells += "," + current_upsell;
                        }
                    }
                    ll("#upsell_product_ids").val(upsellProductIds);
                    ll("#upsell_count").val(upsellCtn);
                    ll("#added_upsells").val(added_upsells);
                    ll("#submit_from_page").val(1);
                    ll('#opt_in_form8').submit();
                }
                lockButton('getUpsell', 2); // unlock just in case as it has finished the request
            });

}

function straightSaleCart(position, page_id) {

    if (!lockButton('straightSaleCart', 1)) {
        return false;
    }

    var url = application_url + "/campaigns/straight.php?callback=?";

    internal = 1;

    var bamdc = 0;
    if (typeof ll("#bamdc").val() != "undefined") {
        bamdc = ll("#bamdc").val();
    }

    var bampc = 0;
    if (typeof ll("#bampc").val() != "undefined") {
        bampc = ll("#bampc").val();
    }

    var promo_activated = 0;
    if (typeof ll("#promo_activated").val() != "undefined") {
        promo_activated = ll("#promo_activated").val();
    }

    var offer_id = ll("#offer_id").val();
    var ykVars = "";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        data: "offer_id=" + offer_id + "&page_id=" + page_id + "&position="
                + position + "&bamdc=" + bamdc + "&bampc=" + bampc
                + "&promo_activated=" + promo_activated,
        success: function (data) {
            ykVars += "shipping_id=" + data.response.shipping_id;
            ykVars += "&straight_sale_pos=" + position;
            ykVars += "&product_id=" + data.response.product_id;
            ykVars += "&added_product_id=" + data.response.added_product_id;
            ykVars += "&added_shipping_id=" + data.response.added_shipping_id;

            var inputs = ll('#opt_in_form8').serializeArray();
            ll.each(inputs, function (i, field) {
                if (field.name != "shipping_id" && field.name != "product_id"
                        && field.name != "straight_sale_pos"
                        && field.name != "added_product_id"
                        && field.name != "added_shipping_id") {
                    ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
                }
            });
            window.location = data.response.secure_url + "order.php?" + ykVars;
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('straightSaleCart', 2);
            ll("#process").html(rush_label);
        }
    });
}

function straightSaleCartLocalLang(position, page_id) {

    if (!lockButton('straightSaleCartLocalLang', 1)) {
        return false;
    }

    var url = application_url + "/campaigns/straight.php?callback=?";
    internal = 1;
    ll("#select-text" + position).html(process_label);
    var bamdc = 0;
    if (typeof ll("#bamdc").val() != "undefined") {
        bamdc = ll("#bamdc").val();
    }

    var bampc = 0;
    if (typeof ll("#bampc").val() != "undefined") {
        bampc = ll("#bampc").val();
    }

    var promo_activated = 0;
    if (typeof ll("#promo_activated").val() != "undefined") {
        promo_activated = ll("#promo_activated").val();
    }

    var offer_id = ll("#offer_id").val();
    var ykVars = "";

    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        data: "offer_id=" + offer_id + "&page_id=" + page_id + "&position="
                + position + "&bamdc=" + bamdc + "&bampc=" + bampc
                + "&promo_activated=" + promo_activated,
        success: function (data) {
            ykVars += "shipping_id=" + data.response.shipping_id;
            ykVars += "&straight_sale_pos=" + position;
            ykVars += "&product_id=" + data.response.product_id;
            ykVars += "&added_product_id=" + data.response.added_product_id;
            ykVars += "&added_shipping_id=" + data.response.added_shipping_id;

            var inputs = ll('#opt_in_form8').serializeArray();
            ll.each(inputs, function (i, field) {
                if (field.name != "shipping_id" && field.name != "product_id"
                        && field.name != "straight_sale_pos"
                        && field.name != "added_product_id"
                        && field.name != "added_shipping_id") {
                    ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
                }
            });
            window.location = data.response.secure_url + "order.php?" + ykVars;
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('straightSaleCartLocalLang', 2);
            ll("#select-text" + position).html(rush_label);
            // not 100% sure if position will bleed to this as it's a callback.
            // shouldn't need a scope buster though as it's not looping
            ll("#process").html(rush_label);
        }
    });
}

function confirmUpsellCart(position, page_id) {

    if (!lockButton('confirmUpsellCart', 1)) {
        return false;
    }

    internal = 1;

    var url = application_url + "/campaigns/straight.php?callback=?";
    var offer_id = ll("#offer_id").val();
    var bamdc = 0;
    if (typeof ll("#bamdc").val() != "undefined") {
        bamdc = ll("#bamdc").val();
    }

    var bampc = 0;
    if (typeof ll("#bampc").val() != "undefined") {
        bampc = ll("#bampc").val();
    }

    var promo_activated = 0;
    if (typeof ll("#promo_activated").val() != "undefined") {
        promo_activated = ll("#promo_activated").val();
    }

    var ykVars = "";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        data: "offer_id=" + offer_id + "&page_id=" + page_id + "&position="
                + position + "&confirm=1&bamdc=" + bamdc + "&bampc=" + bampc
                + "&promo_activated=" + promo_activated,
        success: function (data) {
            ll("#confirm_product_id").val(data.response.product_id);
            ll("#confirm_shipping_id").val(data.response.shipping_id);
            ll("#selected" + position).removeClass("select");
            ll("#product" + position).removeClass("product_unselected");
            ll("#product" + position).addClass("product_selected");
            ll("#selected" + position).addClass("confirmSelected");
            ll("#buy-text" + position).html(process_label);
            ll("#selected" + position).html(data.response.selected_label);
            ll("#confirm_pos").val(position);
            ll("#submit_from_confirm").val(1);
            ll("#confirm_product_id").val(data.response.product_id);
            ll("#confirm_shipping_id").val(data.response.shipping_id);
            ll("#selected" + position).removeClass("select");
            ll("#selected" + position).addClass("confirmSelected");
            ll("#selected" + position).html(data.response.selected_label);
            ll("#confirm_pos").val(position);
            ll("#submit_from_confirm").val(1);
            ll('#opt_in_form10').submit();
            lockButton('confirmUpsellCart', 2);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('confirmUpsellCart', 2)
            ll("#process").html(rush_label);
        }
    });
}

function confirmUpsellCartLocalLang(position, page_id) {

    if (!lockButton('confirmUpsellCartLocalLang', 1)) {
        return false;
    }

    internal = 1;

    var ykVars = "";
    var url = application_url + "/campaigns/straight.php?callback=?";
    var bamdc = 0;
    if (typeof ll("#bamdc").val() != "undefined") {
        bamdc = ll("#bamdc").val();
    }

    var bampc = 0;
    if (typeof ll("#bampc").val() != "undefined") {
        bampc = ll("#bampc").val();
    }

    var promo_activated = 0;
    if (typeof ll("#promo_activated").val() != "undefined") {
        promo_activated = ll("#promo_activated").val();
    }

    var offer_id = ll("#offer_id").val();

    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        data: "offer_id=" + offer_id + "&page_id=" + page_id + "&position="
                + position + "&confirm=1&bamdc=" + bamdc + "&bampc=" + bampc
                + "&promo_activated=" + promo_activated,
        success: function (data) {
            ll("#confirm_product_id").val(data.response.product_id);
            ll("#confirm_shipping_id").val(data.response.shipping_id);
            ll("#selected" + position).removeClass("select");
            ll("#product" + position).removeClass("product_unselected");
            ll("#product" + position).addClass("product_selected");
            ll("#selected" + position).addClass("confirmSelected");
            ll("#buy-text" + position).html(process_label);
            ll("#selected" + position).html(data.response.selected_label);
            ll("#confirm_pos").val(position);
            ll("#submit_from_confirm").val(1);
            ll("#confirm_product_id").val(data.response.product_id);
            ll("#confirm_shipping_id").val(data.response.shipping_id);
            ll("#selected" + position).removeClass("select");
            ll("#selected" + position).addClass("confirmSelected");
            ll("#selected" + position).html(data.response.selected_label);
            ll("#confirm_pos").val(position);
            ll("#submit_from_confirm").val(1);
            ll('#opt_in_form10').submit();
            lockButton('confirmUpsellCartLocalLang', 2)
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('confirmUpsellCartLocalLang', 2)
            ll("#process").html(rush_label);
        }
    });

}

function extraUpsellCart(position, page_id) {

    if (!lockButton('extraUpsellCart', 1)) {
        return false;
    }

    internal = 1;
    var offer_id = ll("#offer_id").val();
    var ykVars = "";
    ll.getJSON("php/straight.php?offer_id=" + offer_id + "&page_id=" + page_id
            + "&position=" + position + "&confirm=1", function (data) {
                ll("#confirm_product_id").val(data.response.product_id);
                ll("#confirm_shipping_id").val(data.response.shipping_id);
                // ll("#selected1").html(data.response.select_label);
                // ll("#selected1").attr("class","select");
                // ll("#selected2").html(data.response.select_label);
                // ll("#selected2").attr("class","select");
                // ll("#selected3").html(data.response.select_label);
                // ll("#selected3").attr("class","select");
                ll("#selected" + position).attr("class", "confirmSelected");
                ll("#selected" + position).html(data.response.selected_label);
                ll("#confirm_pos").val(position);
                ll('#opt_in_form20').submit();
                lockButton('extraUpsellCart', 2);
            });
}

function extraUpsellCartNo() {
    internal = 1;
    ll("#confirm_product_id").val(0);
    ll("#confirm_shipping_id").val(0);
    ll("#confirm_campaign_id").val(0);
    ll("#confirm_pos").val(0);
    ll("#OPT").val("");
    ll('#opt_in_form20').submit();
}

function confirmUpsellSubmit() {
    internal = 1;
    ll("#submit_from_confirm").val(1);
    ll('#opt_in_form10').submit();
}

function removeProtection() {
    internal = 1;
    var package_protection_id;
    var promo_activated = ll("#promo_activated").val();
    var pay_plan_code = 0;
    var boleto_drop_down = 0;
    var exit_activated = 0;
    var step = parseInt(ll("#step").val());

    if (ll("#package_protection_id").is(':checked') == true) {
        package_protection_id = 1;
    } else {
        package_protection_id = 2;
    }

    if (typeof ll("#pay_plan_code").val() != "undefined") {
        pay_plan_code = ll("#pay_plan_code").val();
    }

    if (typeof ll("#exit_activated").val() != "undefined") {
        exit_activated = ll("#exit_activated").val();
    }

    if (typeof ll("#boleto_drop_down option:selected").val() != "undefined") {
        boleto_drop_down = ll("#boleto_drop_down option:selected").val();
    }

    var straight_sale = ll("#straight_sale_pos").val();
    var offer_id = ll("#offer_id").val();
    if (typeof ll("#bamdc").val() != "undefined")
        var bamdc = ll("#bamdc").val();
    else
        var bamdc = "";
    var url = application_url + "/campaigns/protection.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'packageCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "exit_activated=" + exit_activated + "&offer_id=" + offer_id
                + "&boleto_drop_down=" + boleto_drop_down + "&pay_plan_code="
                + pay_plan_code + "&package_protection_id="
                + package_protection_id + "&straight_sale=" + straight_sale
                + "&bamdc=" + bamdc,
        success: function (data) {
            if (boleto_drop_down == 0) {
                ll("#price").html(data.response.prices);
            }
            ll("#offer_total").val(data.response.total);
            ll("#total_amount").html(data.response.currency_total);
            ll("#offer_total_display").html(data.response.currency_total);
            ll("#shipping_promo_price").html(data.response.shipping_price);
            ll("#main_product_price").html(data.response.product_price);
            ll("#shipping_price2").html(data.response.total);
            ll(".shipping_price_replace").html(data.response.total);
            ll("#shipping_price3").html(data.response.total);
            ll(".shipping_change").html(data.response.total);
            ll("#protect_price").html(data.response.currency_protection);

            if (package_protection_id == 2) {
                ll("#package_protection_id").removeAttr("checked");

            } else {
                ll("#package_protection_id").attr("checked", true);
            }
            ll("#package_price_display").html(data.response.currency_protection);
        },
        error: function (e) {
            alert("Network Error Try again Later");
        }
    });

    if (step == 4 || step == 22) { // adding 22 for 2nd upsell stuff
        if (typeof ll("#no_upsell2_package").val() != "undefined") { // check upsell 2 first as the other will probably be hanging out there.
            ll("#no_upsell2_package").val(package_protection_id);
        } else if (typeof ll("#no_upsell_package").val() != "undefined") {
            ll("#no_upsell_package").val(package_protection_id);
        }
    } else if (typeof ll("#no_package_protection").val() != "undefined") {
        ll("#no_package_protection").val(package_protection_id);
    }
}

function removeProtectionMobile() {
    internal = 1;
    var package_protection_id;
    var promo_activated = ll("#promo_activated").val();
    var package1 = parseInt(ll("#checkbox_switch").val());
    var pay_plan_code = 0;
    var boleto_drop_down = 0;
    var exit_activated = 0;
    var step = parseInt(ll("#step").val());

    if (package1 === 1) {
        package_protection_id = 1;
    } else {
        package_protection_id = 2;
    }

    if (typeof ll("#pay_plan_code").val() != "undefined") {
        pay_plan_code = ll("#pay_plan_code").val();
    }

    if (typeof ll("#boleto_drop_down option:selected").val() != "undefined") {
        boleto_drop_down = ll("#boleto_drop_down option:selected").val();
    }

    if ((typeof ll("#exit_activated").val() != "undefined") && step != 4) {
        exit_activated = ll("#exit_activated").val();
    }

    var straight_sale = ll("#straight_sale_pos").val();
    var offer_id = ll("#offer_id").val();

    if (typeof ll("#bamdc").val() != "undefined")
        var bamdc = ll("#bamdc").val();
    else
        var bamdc = "";

    var url = application_url + "/campaigns/protection.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'packageCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "offer_id=" + offer_id + "&exit_activated=" + exit_activated
                + "&boleto_drop_down=" + boleto_drop_down + "&pay_plan_code="
                + pay_plan_code + "&package_protection_id="
                + package_protection_id + "&straight_sale=" + straight_sale
                + "&bamdc=" + bamdc,
        success: function (data) {
            if (boleto_drop_down == 0) {
                ll("#price").html(data.response.prices);
            }
            ll("#offer_total").val(data.response.total);
            ll("#total_amount").html(data.response.currency_total);
            ll("#offer_total_display").html(data.response.currency_total);
            ll("#shipping_promo_price").html(data.response.shipping_price);
            ll("#main_product_price").html(data.response.product_price);
            ll("#shipping_price2").html(data.response.total);
            ll(".shipping_price_replace").html(data.response.total);
            ll("#shipping_price3").html(data.response.total);
            ll("#protect_price").html(data.response.currency_protection);

            if (package_protection_id == 2) {
                ll("#package_protection_id").removeAttr("checked");

            } else {
                ll("#package_protection_id").attr("checked", true);
            }
            ll("#package_price_display").html(data.response.currency_protection);
        },
        error: function (e) {
            alert("Network Error Try again Later");
        }
    });

    if (step == 4 || step == 22) {
        if (typeof ll("#no_upsell2_package").val() != "undefined") {
            ll("#no_upsell2_package").val(package_protection_id);
        } else if (typeof ll("#no_upsell_package").val() != "undefined") {
            ll("#no_upsell_package").val(package_protection_id);
        }
    } else if (typeof ll("#no_package_protection").val() != "undefined") {
        ll("#no_package_protection").val(package_protection_id);
    }
}

function toggleBillingAddress(radioButtonObj) {
    internal = 1;
    var billingDiv = document.getElementById('billingDiv');

    if (radioButtonObj.value == "no") {
        billingDiv.style.display = 'inline';
    } else {
        billingDiv.style.display = 'none';
    }
}

function validEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function isValidEmail(str, label_fields) {
    var at = "@";
    var dot = ".";
    var lat = str.indexOf(at);
    var lldot = str.lastIndexOf(dot);
    var lstr = str.length;
    var ldot = str.indexOf(dot);

    if (str.indexOf(at) == -1) {
        alert(label_fields[14] + " " + label_fields[8]);
        return false;
    }

    if (str.indexOf(at) == -1 || str.indexOf(at) == 0
            || str.indexOf(at) == (lstr - 1) || str.indexOf(at) == (lstr - 3)) {
        alert(label_fields[14] + " " + label_fields[8]);
        return false;
    }

    if (lldot == lstr - 1 || lldot == lstr - 2
            || (lldot == ldot && lldot < lat)) {
        alert(label_fields[14] + " " + label_fields[8]);
        return false;
    }

    if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0
            || str.indexOf(dot) == (lstr - 1)) {
        alert(label_fields[14] + " " + label_fields[8]);
        return false;
    }

    if (str.indexOf(at, (lat + 1)) != -1) {
        alert(label_fields[14] + " " + label_fields[8]);
        return false;
    }

    if (str.substring(lat - 1, lat) == dot
            || str.substring(lat + 1, lat + 2) == dot) {
        alert(label_fields[14] + " " + label_fields[8]);
        return false;
    }

    if (str.indexOf(dot, (lat + 2)) == -1) {
        alert(label_fields[14] + " " + label_fields[8]);
        return false;
    }

    if (str.indexOf(" ") != -1) {
        alert(label_fields[14] + " " + label_fields[8]);
        return false;
    }
    var isDoubleByte = false;
    for (var i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) > 127) {
            isDoubleByte = true;
            break;
        }
    }

    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (!isDoubleByte && reg.test(str) == false) {
        alert(label_fields[14] + " " + label_fields[8]);
        return false;
    }
    return true;
}

function allValidChars(email) {
    var parsed = true;
    var validchars = "abcdefghijklmnopqrstuvwxyz0123456789@.-_";
    for (var i = 0; i < email.length; i++) {
        var letter = email.charAt(i).toLowerCase();
        if (validchars.indexOf(letter) != -1)
            continue;
        parsed = false;
        break;
    }
    return parsed;
}

function form_validator(label_fields, validate_field, validate_field_label) {

    internal = 1;
    if (ll('#hasFormSubmitted').val() != '') {
        var prevPost = new Date(ll('#hasFormSubmitted').val());
        prevPost.setSeconds(prevPost.getSeconds() + 3);
        var currentTime = new Date();
        if (prevPost < currentTime) {
            // -- blank out the formsubmitted because user went back and forth
            // and hidden values still cached
            ll('#hasFormSubmitted').val('');
        }
    }

    var empty_fields = new Array();
    var empty_field_num = new Array();
    var empty_count = 0;
    var str = label_fields[28];

    for (i = 0; i < validate_field.length; i++) {
        var field_name = validate_field[i];

        var field_ref = ll("#" + field_name);
        // alert("in here: "+i+" -- "+field_name);
        if ((field_name == 'cc_number' || field_name == 'cc_expires' || field_name == 'cc_cvv')
                && (ll('#payment_method_type').val() != 1)) {
            continue;
        }

        if (field_name == 'cc_number') {
            if (ll("#" + field_name) != undefined) {
                if (ll("#" + field_name).val().length < 13) {
                    alert(label_fields[14] + " " + label_fields[10]);
                    return false;
                }
            }
        }

        if (ll('#payment_method_type') == 3) {
            // alert("validating checking?");
            // -- checking account validation
            field_ref = ll("#routing_number");
            if ((field_ref.val().length == 0) || (field_ref.val() == null) || (field_ref.val() == '')) {
                alert(label_fields[18]);
                field_ref.focus();
                return false;
            }
            field_ref = ll('#account_number');
            if ((field_ref.val().length == 0) || (field_ref.val() == null) || (field_ref.val() == '')) {
                alert(label_fields[19]);
                field_ref.focus();
                return false;
            }
        }

        if (field_name == 'cc_expires') {
            var exp_month = ll("#fields_expmonth option:selected").val();
            var exp_year = ll("#fields_expyear option:selected").val();

            if ((exp_month.length == 2) && (exp_year.length == 2)) {
                ll("#cc_expires").val(exp_month + exp_year);
                field_ref = ll("#cc_expires");
            }
            // alert(ll("#cc_expires").val()+"--expired");
        }

        if ((field_name == 'cc_cvv') && (ll("#ignore_cvv").val() == 0)) {
            var cc_cvv = ll('#cc_cvv').val();
            if ((typeof ll('#cc_type :selected').val() != "undefined")
                    && (ll('#cc_type :selected').val() == 19)
                    && (ll("#ignore_cvv").val() == 0)) {
                if (cc_cvv.length <= 2 || cc_cvv.length > 3) {
                    // alert(label_fields[12]);
                    showCvvWhat();
                    field_ref.focus();
                    return false;
                }
            } else if ((cc_cvv.length <= 2 || cc_cvv.length > 4)
                    && (ll("#ignore_cvv").val() == 0)) {
                alert(label_fields[12]);
                showCvvWhat();
                field_ref.focus();
                return false;
            }
        }

        // catch all spaces, this is invalid
        var tempStr = ll.trim(field_ref.val());

        if (((tempStr.length == 0) || (tempStr == ' ')) && (field_name != "cc_cvv")) {
            empty_fields[empty_count] = validate_field_label[i];
            empty_field_num[empty_count] = i;
            empty_count++;
        }
        if (((field_ref.length == 0) || (field_ref.val() == null) || (field_ref.val() == '')) && (field_name != "cc_cvv")) {
            empty_fields[empty_count] = validate_field_label[i];
            empty_field_num[empty_count] = i;
            empty_count++;
        }
    }

    for (x in empty_fields) {
        str = str + '<br/>' + empty_fields[x];
        if (empty_fields[x] != undefined) {
            alert(label_fields[15] + ' ' + empty_fields[x] + '');
            ll("#" + validate_field[empty_field_num[x]]).focus();
        } else {
            alert(label_fields[16]);
        }
        return false;
    }

    elTermsRequired = ll('input[name=terms_required]').last();
    if (!elTermsRequired.length) {
        elTermsRequired = ll('#terms_required');
    }
    elTerms = ll('#terms');
    //TODO: find why back-end sending wrong settings to front-end
    if (elTermsRequired.val() == 1 && elTerms.length) {
        if (elTerms.is(':checked') == false) {
            alert(label_fields[20]);
            return false;
        }
    }

    if (ll('#radioTwo') != undefined) {
        var radio2Obj = ll('#radioTwo');
        if (radio2Obj.checked) {
            field_ref = ll('#billing_street_address');
            if ((field_ref.val().length == 0) || (field_ref.val() == null)
                    || (field_ref.val() == '')
                    || (ll.trim(field_ref.val()) == '')) {
                alert(label_fields[21]);
                return false;
            }
            field_ref = ll('#billing_city');
            if ((field_ref.val().length == 0) || (field_ref.val() == null)
                    || (field_ref.val() == '')
                    || (ll.trim(field_ref.val()) == '')) {
                alert(label_fields[22]);
                return false;
            }
            var state_select_is_visible = (ll('#billing_state').is(':visible') == true);
            field_ref = ll('#billing_state');
            if (state_select_is_visible) {
                if ((field_ref.val().length == 0) || (field_ref.val() == null)
                        || (field_ref.val() == '')
                        || (ll.trim(field_ref.val()) == '')) {
                    alert(label_fields[23]);
                    return false;
                }
            }
            var region_is_visible = (ll('#billing_state2').is(':visible') == true);
            field_ref = ll('#billing_state2');
            if (region_is_visible) {
                if ((field_ref.val().length == 0) || (field_ref.val() == null)
                        || (field_ref.val() == '')
                        || (ll.trim(field_ref.val()) == '')) {
                    alert(label_fields[23]);
                    return false;
                } else {
                    // store the international region value
                    ll(new Option(ll('#billing_state2').val(), ll('#billing_state2').val()))
                            .appendTo('#billing_state').attr('selected', 'selected');
                }
            }

            field_ref = ll('#billing_postcode');
            if ((field_ref.val().length == 0) || (field_ref.val() == null)
                    || (field_ref.val() == '')
                    || (ll.trim(field_ref.val()) == '')) {
                alert(label_fields[24]);
                return false;
            }
            if (ll('#billing_country') != undefined) {
                field_ref = ll('#billing_country');
                if ((field_ref.val().length == 0) || (field_ref.val() == null)
                        || (field_ref.val() == '')
                        || (ll.trim(field_ref.val()) == '')) {
                    alert(label_fields[25]);
                    return false;
                }
            }
        }
    }

    if (empty_count != 0) {
        // var diverr=ll('#err');
        // diverr.innerHTML='';
        ll('#err').html(str);
    } else {
        if (ll('#fields_email').is(':visible') == true) {
            var emailCheck = isValidEmail(ll("#fields_email").val(),
                    label_fields);
        } else {
            var emailCheck = true;
        }
        if (emailCheck) {
            return true;
        }
    }

    return false;
}

function onlyNumbers(e) {
    var keynum;
    var keychar;
    var numcheck;
    if (window.event) // IE
    {
        keynum = e.keyCode;
    } else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    keychar = String.fromCharCode(keynum);
    numcheck = /\d/;

    switch (keynum) {
        case 8: // backspace
        case 9: // tab
        case 35: // end
        case 36: // home
        case 37: // left arrow
        case 38: // right arrow
        case 39: // insert
        case 45: // delete
        case 46: // 0
        case 48: // 1
        case 49: // 2
        case 50: // 3
        case 51: // 4
        case 52: // 5
        case 54: // 6
        case 55: // 7
        case 56: // 8
        case 57: // 9
        case 96: // 0
        case 97: // 1
        case 98: // 2
        case 99: // 3
        case 100: // 4
        case 101: // 5
        case 102: // 6
        case 103: // 7
        case 104: // 8
        case 105: // 9
            result2 = true;
            break;
        case 109: // dash -
            result2 = true;
            break;
        default:
            result2 = numcheck.test(keychar);
            break;
    }

    return result2;
}

function showWhatIs() {
    document.getElementById("protect_what_is").className = "show";
}
function hideWhatIs() {
    document.getElementById("protect_what_is").className = "hide";
}

function getCompleteUpsell(approved) {
    internal = 1;
    if (approved == 1) {
        ll("#iwantthat").val(1);
    } else {
        ll("#iwantthat").val(0);
    }
    ll('#yknot_upsell_form').submit();
}

function submitCompleteUpsell() {
    internal = 1;
    ll('#submit_from_page').val(1);
    ll('#from_upsell_submit').val(1);
    ll('#yknot_upsell_form').submit();
}

// functions for payment type
var slideUpDelay = 200;
var slideDownDelay = 1000;

function paymentIsThere(payment_type) {

    ll('#payment_method_type').val(payment_type);

    if (typeof ll('#creditCardInput').val() !== "undefined") {
        ll('#creditCardInput').attr("checked", false);
        ll('#creditCardInfo').delay(slideUpDelay).slideUp();
    }

    if (typeof ll('#paypalInput').val() !== "undefined") {
        ll('#paypalInfo').delay(slideUpDelay).slideUp();
        ll('#paypalInput').attr("checked", false);
    }

    if (typeof ll('#directDebitInput').val() !== "undefined") {
        ll('#directDebtInfo').delay(slideUpDelay).slideUp();
        ll('#directDebitInput').attr("checked", false);
    }

    if (typeof ll('#iDealInput').val() !== "undefined") {
        ll('#iDealInput').attr("checked", false);
        ll('#iDealInfo').delay(slideUpDelay).slideUp();
    }

    if (typeof ll('#sofortInput').val() !== "undefined") {
        ll('#sofortInput').attr("checked", false);
        ll('#sofortInfo').delay(slideUpDelay).slideUp();
    }

    if (typeof ll('#giroInput').val() !== "undefined") {
        ll('#giroInput').attr("checked", false);
        ll('#giroInfo').delay(slideUpDelay).slideUp();
    }

    if (typeof ll('#epsInput').val() !== "undefined") {
        ll('#epsInput').attr("checked", false);
        ll('#epsInfo').delay(slideUpDelay).slideUp();
    }

    if (typeof ll('#debitcardInput').val() !== "undefined") {
        ll('#debitcardInput').attr("checked", false);
        ll('#debitcardInfo').delay(slideUpDelay).slideUp();
    }

    if (typeof ll('#bpayInput').val() !== "undefined") {
        ll('#bpayInput').attr("checked", false);
        ll('#bpayInfo').delay(slideUpDelay).slideUp();
    }

    if (typeof ll('#eftInput').val() !== "undefined") {
        ll('#eftInput').attr("checked", false);
        ll('#eftInfo').delay(slideUpDelay).slideUp();
    }

    if (typeof ll('#polipayInput').val() !== "undefined") {
        ll('#polipayInput').attr("checked", false);
        ll('#polipayInfo').delay(slideUpDelay).slideUp();
    }

    if (payment_type == 1) {
        ll('#creditCardInfo').delay(slideDownDelay).slideDown();
        ll('#creditCardInput').attr("checked", true);
    } else if (payment_type == 2) {
        ll('#paypalInfo').delay(slideDownDelay).slideDown();
        ll('#paypalInput').attr("checked", true);
    } else if (payment_type == 3) {
        ll('#directDebitInput').attr("checked", true);
        ll('#directDebitInfo').delay(slideDownDelay).slideDown();
    } else if (payment_type == 4) {
        ll('#iDealInput').attr("checked", true);
        ll('#iDealInfo').delay(slideDownDelay).slideDown();
    } else if (payment_type == 5) {
        ll('#sofortInput').attr("checked", true);
        ll('#sofortInfo').delay(slideDownDelay).slideDown();
    } else if (payment_type == 6) {
        ll('#giroInput').attr("checked", true);
        ll('#giroInfo').delay(slideDownDelay).slideDown();
    } else if (payment_type == 7) {
        ll('#epsInput').attr("checked", true);
        ll('#epsInfo').delay(slideDownDelay).slideDown();
    } else if (payment_type == 8) {
        ll('#debitcardInput').attr("checked", true);
        ll('#debitcardInfo').delay(slideDownDelay).slideDown();
    } else if (payment_type == 9) {
        ll('#bpayInput').attr("checked", true);
        ll('#bpayInfo').delay(slideDownDelay).slideDown();
    } else if (payment_type == 10) {
        ll('#eftInput').attr("checked", true);
        ll('#eftInfo').delay(slideDownDelay).slideDown();
    } else if (payment_type == 13) {
        ll('#polipayInput').attr("checked", true);
        ll('#polipayInfo').delay(slideDownDelay).slideDown();
    } else if (payment_type == 16) {
        updateBoleto();
    }
}

// cvv help image
function showCvvWhat() {
    ll('#cvv_whatis_holder').removeClass().addClass('show');
}
function hideCvvWhat() {
    ll('#cvv_whatis_holder').removeClass().addClass('hide');
}

function showDownsell(downsell_html, down_css) {
    internal = 1;
    ll(".downsell_display").html(downsell_html);
    ll("<style type='text/css'>" + down_css + "</style>").appendTo("head");
    ll(".downsell_overlay, .downsell_display").show();
}

function doDownSell(approved, product_id, shipping_id) {

    if (!lockButton('doDownSell', 1)) {
        return false;
    }

    internal = 1;
    var ykVars = "";
    var inputs = ll('#opt_in_form2').serializeArray();
    var offer_id = ll("#offer_id").val();

    if ((ll("#package_protection_id").length > 0)
            && (ll("#package_protection_id").is(':checked') == true)) {
        ykVars += "&package_protection_id="
                + ll("#package_protection_id").val();
    }

    ll.each(inputs, function (i, field) {
        if ((field.name != "cc_number") && (field.name != "cc_cvv")) {
            if (field.name == "AFID") {
                ykVars += "&AFID=DISC";
            } else {
                ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
            }
        }
    });
    var cc = encodeURI(encryptData(ll("#cc_number").val()));
    var cv2 = encodeURI(encryptData(ll("#cc_cvv").val()));
    cc = cc.replace(/\+/g, "%2B");
    cv2 = cv2.replace(/\+/g, "%2B");

    ykVars += "&cc_number=" + cc;
    ykVars += "&cc_cvv=" + cv2;
    ll(".downsell_overlay, .downsell_display").hide();

    var attempts = ll("#cc_attempts").val();
    if ((approved == 1) && (product_id > 0)) {

        var url = application_url + "/campaigns/languages.php?callback=?";
        ll.ajax({
            type: 'POST',
            url: url,
            async: false,
            jsonpCallback: 'processCallBack',
            contentType: "application/json",
            dataType: 'jsonp',
            data: "step=" + step + "&offer_id=" + offer_id,
            success: function (data) {

                ll("#rush_order_btn1").html(data.processing_css);
                ll("#rush_order_btn2").html(data.processing_css2);
                downSellSuccess(offer_id, attempts, ykVars, product_id, shipping_id);
            },
            error: function (e) {
                alert("Network Error Try again Later");
                lockButton('doDownSell', 2);
                ll("#rush_order_btn1").html("<span id='process'>" + rush_label + "</span>");
            }
        });

    } else {
        lockButton('doDownSell', 2);
        window.location = "order.php?" + ykVars;
    }
}

function downSellSuccess(offer_id, attempts, ykVars, product_id, shipping_id) {
    attempts = attempts + 1;
    ykVars += "&downsell_discount_id=7&down_sell_product_id=" + product_id
            + "&is_downsell=1&down_sell_shipping_id=" + shipping_id;

    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        data: ll('#opt_in_form').serializeArray(),
        success: function (data) {

            ll("#cc_attempts").val(1);

            if ((data.response.payment_method_type == 4)
                    && (data.response.redirect_url.length > 0)) {
                window.location = data.response.redirect_url;
            } else {
                var order_id = data.response.fields.order_id;
                var customer_id = data.response.fields.customer_id;
                var order_total = data.response.fields.order_total;
                var gateway_desc = data.response.fields.gatewayDesc;
                var is_upsell = data.response.is_upsell;
                var upsell_position = data.response.upsell_position;
                var package_order_id = data.response.fields.package_order_id;
                var package_customer_id = data.response.fields.package_customer_id;
                var package_number = data.response.fields.package_number;
                var package_gateway = data.response.fields.package_gateway;

                if (is_upsell == 1
                        && (upsell_position == 1 || upsell_position == 3)) {
                    window.location = "upsell.php?ykPage=1&gateway_desc="
                            + gateway_desc
                            + "&order_id="
                            + order_id
                            + "&customer_id="
                            + customer_id
                            + "&order_total="
                            + order_total
                            + "&package_order_id="
                            + package_order_id
                            + "&package_customer_id="
                            + package_customer_id
                            + "&package_number="
                            + package_number
                            + "&package_gateway="
                            + package_gateway + ykVars;
                } else {
                    window.location = "confirm.php?gateway_desc="
                            + gateway_desc + "&order_id=" + order_id
                            + "&customer_id=" + customer_id
                            + "&order_total=" + order_total
                            + "&package_order_id=" + package_order_id
                            + "&package_customer_id="
                            + package_customer_id + "&package_number="
                            + package_number + "&package_gateway="
                            + package_gateway + ykVars;
                }
            }
        },
        error: function (e) {
            ll("#cc_attempts").val(attempts);
            var msg = data.response.fields.error;
            alert("Error: " + msg);
            lockButton('doDownSell', 2);
            ll("#rush_order_btn1").html("<span id='process'>" + data.processing_rush + "</span>");
            ll("#rush_order_btn2").html("<span id='process'>" + data.processing_rush2 + "</span>");
        }
    });
}

function encryptData(toencrypt) {
    var pem = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApqzWwdxsDAaP7yDFdrWU4kXYtobh5SfyrLf6L2OKLeQFRFVklA+dwLi32AkM8VgZeYJrCHiE+YZcHfL5SkbsYjy34OnIemnZn/TI1AkqHpwb7SmDRJn/XwOGRYBUkXyCltmIszTLc2JIP9foXoaoGI019KmBPVFPF6O66NxVGscnTd3XeAxiK9svw2kvaDzvkpemcY2fiOlgRPevuY95WXPH88OgS6t3eLvPj+CTxYqKW3uMp3FXrMKWGz+ZyQ9VDwbM8uwPSkL1LbiScN6QchwtyleLaWF7xa1DXlB0QNhL1mwsAdAzVAZ8+HfE+RXQAVr/a/kX+sI5vXG74Tb7mwIDAQAB-----END PUBLIC KEY-----";
    var ekey = RSA.getPublicKey(pem);
    return RSA.encrypt(toencrypt, ekey);
}

function doIndexExit() {
    // internal=1;
    // ll("#exit_activated").val(1);
    // ll("#exit").hide();
    // ll("#index_exit_header").show();
    internal = 1;
    ll("#exit_activated").val(1);
    ll("#popup").hide();
    ll("#index_exit_header").show();
}

function doOrderExit() {

    if (!lockButton('doOrderExit', 1)) {
        return false;
    }

    internal = 1;
    ll("#exit_activated").val(1);
    ll("#popup").hide();
    ll("#order_exit_header").show();

    var ykVars = "";
    var inputs = ll('#opt_in_form2').serializeArray();
    var offer_id = ll("#offer_id").val();

    if ((ll("#package_protection_id").length > 0)
            && (ll("#package_protection_id").is(':checked') == true)) {
        ykVars += "&package_protection_id="
                + ll("#package_protection_id").val();
    }

    ll.each(inputs, function (i, field) {
        if ((field.name != "cc_number") && (field.name != "cc_cvv")) {
            if (field.name == "AFID") {
                ykVars += "&AFID=DISC";
            } else {
                ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
            }
        }
    });
    var cc = encodeURI(encryptData(ll("#cc_number").val()));
    var cv2 = encodeURI(encryptData(ll("#cc_cvv").val()));
    cc = cc.replace(/\+/g, "%2B");
    cv2 = cv2.replace(/\+/g, "%2B");

    ykVars += "&cc_number=" + cc;
    ykVars += "&cc_cvv=" + cv2;

    var url = application_url + "/campaigns/exit.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'exitCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: ykVars,
        success: function (data) {
            // ll("body").prepend(data.response.exit_header);
            ll("#offerPrice").html(data.response.price_form);
            ll("#offer_total").html(data.response.offer_total);
            ll("#shipping_price2").html(data.response.shipping_price);
            ll(".shipping_price_replace").html(data.response.shipping_price);
            ll("#shipping_price3").html(data.response.shipping_price);
            lockButton('doOrderExit', 2);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('doOrderExit', 2);
        }
    });
}

var cleared_names = [];

function oldDoExitForm() {
    var filename = location.pathname.substring(location.pathname.lastIndexOf('/') + 1);
    var inputs = "";
    var ykVars = "";
    if ((filename.toLowerCase() == "index.php") || (filename.length == 0)) {
        inputs = ll('#opt_in_form').serializeArray();
    } else {
        inputs = ll('#opt_in_form8').serializeArray();
    }
    var offer_id = ll("#offer_id").val();

    ll.each(inputs, function (i, field) {
        ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
    });
    ykVars += "&bamdc=1";

    window.location = "special.php?" + ykVars;
}

function clearText(theid, position_ref) {
    var emailtext = ll("#" + theid).val();

    if (emailtext.indexOf("@") > 0) {
    } else {

        if ((ll.inArray(theid, cleared_names)) === -1) {
            ll("#" + theid).val('');
            cleared_names.push(theid);
        }
    }

    var refer_positions = ll("#refer_positions").val();

    if (refer_positions.length == 0) {
        ll("#refer_positions").val(position_ref);
    } else {
        var positions_refs = refer_positions.split(",");
        var found = false;

        for (var c = 0; c < positions_refs.length; c++) {

            if (positions_refs[c] == position_ref) {
                found = true;
                break;
            }
        }

        if (found == false) {
            refer_positions = refer_positions + "," + position_ref;
            ll("#refer_positions").val(refer_positions);
        }
    }
}

function validateReferAFriend() {
    var positions = ll("#refer_positions").val();
    var allpositions = positions;
    var changed = positions.split(",");
    for (temp in changed) {
        var a = (parseInt(temp) + 1);
        var email = ll("#referralEmail" + a).val();
        if (validEmail(email) == false) {
            ll("#refer_positions").val(allpositions);
            ll("#referralEmail" + a).focus();
            return false;
        }
    }
    return true;
}

function referAFriend() {
    if (validateReferAFriend() == true) {
        var name1 = ll("#referralName1").val();
        var email1 = ll("#referralEmail1").val();
        var name2 = ll("#referralName2").val();
        var email2 = ll("#referralEmail2").val();
        var name3 = ll("#referralName3").val();
        var email3 = ll("#referralEmail3").val();
        var name4 = ll("#referralName4").val();
        var email4 = ll("#referralEmail4").val();
        var name5 = ll("#referralName5").val();
        var email5 = ll("#referralEmail5").val();
        var firstName = ll("#fields_fname").val();
        var lastName = ll("#fields_lname").val();
        var email = ll("#fields_email").val();
        var offer_id = ll("#offer_id").val();
        var friend_id = ll("#rafriend_id").val();
        var ykVars = "&name1=" + name1 + "&email1=" + email1 + "&name2="
                + name2 + "&email2=" + email2 + "&name3=" + name3 + "&email3="
                + email3 + "&name4=" + name4 + "&email4=" + email4 + "&name5="
                + name5 + "&email5=" + email5 + "&step=99&rt=1&firstName="
                + firstName + "&lastName=" + lastName + "&email=" + email
                + "&offer_id=" + offer_id + "&friend_id=" + friend_id;
        var url = application_url + '/campaigns/friend.php?callback=?' + ykVars;
        ll.ajax({
            type: 'POST',
            url: url,
            async: false,
            jsonpCallback: 'referCallBack',
            contentType: content_type_text,
            dataType: 'jsonp',
            success: function (json) {
                if (json.settings[0].status == 100) {
                    ll("#friend_id").html(json.settings[0].message);
                    ll("#friend_sent").val(1);
                } else {
                    alert(json.settings[0].error);
                }
            },
            error: function (e) {
                alert("Network Error Try again Later");
            }
        });

    } else {
        alert("Invalid Email");
    }
}

function changeProduct(position) {

    var bamdc = 0;
    if (typeof ll("#bamdc").val() != "undefined") {
        bamdc = ll("#bamdc").val();
    } else {
        bamdc = 0;
    }

    var offer_id = ll("#offer_id").val();

    ll.getJSON("php/order.php?offer_id=" + offer_id + "&position=" + position + "&bamdc=" + bamdc, function (data) {
        switch (position) {
            case 1:
                ll('#product1').removeClass('product_unselected');
                ll('#product1').addClass('product_selected');
                ll('#add_button1').removeClass('add_button');
                ll('#add_button1').addClass('add_button_selected');
                ll('#add_button1').text(data.response.selected_label);
                ll('#product2').removeClass('product_selected');
                ll('#product2').addClass('product_unselected');
                ll('#add_button2').removeClass('add_button_selected');
                ll('#add_button2').addClass('add_button');
                ll('#add_button2').text(data.response.selected_button_label);
                ll('#product3').removeClass('product_selected');
                ll('#product3').addClass('product_unselected');
                ll('#add_button3').removeClass('add_button_selected');
                ll('#add_button3').addClass('add_button');
                ll('#add_button3').text(data.response.selected_button_label);
                ll('#product_img').attr('src', data.response.product_image_url);
                ll('#product_name').html(data.response.product_name);
                ll('#retailPrice').html(data.response.retail_price);
                ll('#saved_amount').html(data.response.saved_amount);
                ll('#p_price').html(data.response.product_price);
                ll('#shipping_price').html(data.response.shipping_price);
                ll('#total_amount').html(data.response.total);
                break;
            case 2:
                ll('#product1').removeClass('product_selected');
                ll('#product1').addClass('product_unselected');
                ll('#add_button1').removeClass('add_button_selected');
                ll('#add_button1').addClass('add_button');
                ll('#add_button1').text(data.response.selected_button_label);
                ll('#product2').removeClass('product_unselected');
                ll('#product2').addClass('product_selected');
                ll('#add_button2').removeClass('add_button');
                ll('#add_button2').addClass('add_button_selected');
                ll('#add_button2').text(data.response.selected_label);
                ll('#product3').removeClass('product_selected');
                ll('#product3').addClass('product_unselected');
                ll('#add_button3').removeClass('add_button_selected');
                ll('#add_button3').addClass('add_button');
                ll('#add_button3').text(data.response.selected_button_label);
                ll('#product_img').attr('src', data.response.product_image_url);
                ll('#product_name').html(data.response.product_name);
                ll('#retailPrice').html(data.response.retail_price);
                ll('#saved_amount').html(data.response.saved_amount);
                ll('#p_price').html(data.response.product_price);
                ll('#shipping_price').html(data.response.shipping_price);
                ll('#total_amount').html(data.response.total);

                break;
            case 3:
                ll('#product1').removeClass('product_selected');
                ll('#product1').addClass('product_unselected');
                ll('#add_button1').removeClass('add_button_selected');
                ll('#add_button1').addClass('add_button');
                ll('#add_button1').text(data.response.selected_button_label);
                ll('#product2').removeClass('product_selected');
                ll('#product2').addClass('product_unselected');
                ll('#add_button2').removeClass('add_button_selected');
                ll('#add_button2').addClass('add_button');
                ll('#add_button2').text(data.response.selected_button_label);
                ll('#product3').removeClass('product_unselected');
                ll('#product3').addClass('product_selected');
                ll('#add_button3').removeClass('add_button');
                ll('#add_button3').addClass('add_button_selected');
                ll('#add_button3').text(data.response.selected_label);
                ll('#product_img').attr('src', data.response.product_image_url);
                ll('#product_name').html(data.response.product_name);
                ll('#retailPrice').html(data.response.retail_price);
                ll('#saved_amount').html(data.response.saved_amount);
                ll('#p_price').html(data.response.product_price);
                ll('#shipping_price').html(data.response.shipping_price);
                ll('#total_amount').html(data.response.total);
                break;
        }

        ll("#protect_price").val(data.response.currency_protection);
        ll("#offer_total").val(data.response.total);
        ll("#shipping_id").val(data.response.shipping_id);
        ll("#straight_sale_pos").val(position);
        ll("#product_id").val(data.response.product_id);
        ll("#added_product_id").val(data.response.added_product_id);
        ll("#added_shipping_id").val(data.response.added_shipping_id);
    });
}

function ValidateExpDate() {

    var ccExpYear = ll('#fields_expyear :selected').val();
    var ccExpMonth = ll('#fields_expmonth :selected').val();

    if (ccExpYear.length == 0 || ccExpMonth.length == 0) {
        alert("Expiration Date");
        return false;
    } else {
        return true;
    }
}

function setCcType(cc_name) {
    var full_name = "";
    ll("#select_payment_type_" + cc_name).attr('checked', true);
    if ((cc_name == 2) || (cc_name == 10) || (cc_name == 13) || (cc_name == 14)
            || (cc_name == 16) || (cc_name == 17) || (cc_name == 18)
            || (cc_name == 19)) {
        full_name = "visa-" + cc_name;
    } else if ((cc_name == 3) || (cc_name == 8) || (cc_name == 11)
            || (cc_name == 22)) {
        full_name = "master-" + cc_name;
    } else if ((cc_name == 4) || (cc_name == 20) || (cc_name == 21)) {
        full_name = "discover-" + cc_name;
    }

    ll("#cc_type").val(full_name);

    if ((cc_name == 8) || (cc_name == 14) || (cc_name == 10) || (cc_name == 11)) {
        ll("#ignore_cvv").val(1);
        ll("#ignore_cvv").bind();
        ll("#cc_cvv_tr").hide();
    } else {
        ll("#ignore_cvv").val(0);
        ll("#ignore_cvv").bind();
        ll("#cc_cvv_tr").show();
    }
}

function updateShippingField(divid) {

}

function showPromoHeader() {
    ll("#promo-code-container").delay(500).slideDown();
}

function submitBoleto() {

    if (!lockButton('submitBoleto', 1)) {
        return false;
    }

    var url = application_url + '/campaigns/boleto.php?callback=?';
    var ykVars = "";
    var inputs = ll('#opt_in_form2').serializeArray();

    if ((ll("#package_protection_id").length > 0)
            && (ll("#package_protection_id").is(':checked') == true)) {
        ykVars += "&package_protection_id="
                + ll("#package_protection_id").val();
    }

    ll.each(inputs, function (i, field) {
        if ((field.name != "cc_number") && (field.name != "cc_cvv")) {
            ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
        }
    });
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        data: ll('#opt_in_boleto').serializeArray(),
        success: function (data) {

            if (data.response.status == "success") {

                ll("#cc_attempts").val(1);

                if ((ll.inArray(data.response.payment_method_type,
                        payment_methods))
                        && (typeof data.response.fields.redirect_url != "undefined")
                        && (data.response.fields.redirect_url.length > 0)) {
                    window.location = data.response.fields.redirect_url;
                } else {
                    var order_id = data.response.fields.order_id;
                    var customer_id = data.response.fields.customer_id;
                    var order_total = data.response.fields.order_total;
                    var gateway_desc = data.response.fields.gatewayDesc;
                    var is_upsell = data.response.is_upsell;
                    var upsell_position = data.response.upsell_position;
                    var package_order_id = data.response.fields.package_order_id;
                    var package_customer_id = data.response.fields.package_customer_id;
                    var package_number = data.response.fields.package_number;
                    var package_gateway = data.response.fields.package_gateway;

                    if (is_upsell == 1 && (upsell_position == 1 || upsell_position == 3)) {
                        window.location = "upsell.php?ykPage=1&gateway_desc="
                                + gateway_desc
                                + "&order_id="
                                + order_id
                                + "&customer_id="
                                + customer_id
                                + "&order_total="
                                + order_total
                                + "&package_order_id="
                                + package_order_id
                                + "&package_customer_id="
                                + package_customer_id
                                + "&package_number="
                                + package_number
                                + "&package_gateway="
                                + package_gateway
                                + ykVars;
                    } else {
                        window.location = "confirm.php?gateway_desc="
                                + gateway_desc + "&order_id="
                                + order_id + "&customer_id="
                                + customer_id + "&order_total="
                                + order_total + "&package_order_id="
                                + package_order_id
                                + "&package_customer_id="
                                + package_customer_id
                                + "&package_number=" + package_number
                                + "&package_gateway=" + package_gateway
                                + ykVars;
                    }
                }
            } else {
                alert(data.response.fields.error);
                lockButton('submitBoleto', 2);
            }
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitBoleto', 2);
        }
    });
}

function submitInstallment() {

    if (!lockButton('submitInstallment', 1)) {
        return false;
    }

    var url = application_url + '/campaigns/installments.php?callback=?';
    var ykVars = "";
    var inputs = ll('#opt_in_form2').serializeArray();

    if ((ll("#package_protection_id").length > 0)
            && (ll("#package_protection_id").is(':checked') == true)) {
        ykVars += "&package_protection_id="
                + ll("#package_protection_id").val();
    }

    ll.each(inputs, function (i, field) {
        if ((field.name != "cc_number") && (field.name != "cc_cvv")) {
            ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
        }
    });
    var cc = encodeURI(encryptData(ll("#cc_number").val()));
    var cv2 = encodeURI(encryptData(ll("#cc_cvv").val()));

    var cc = cc.replace(/\+/g, "%2B");
    var cv2 = cv2.replace(/\+/g, "%2B");

    ykVars += "&cc_number=" + cc;
    ykVars += "&cc_cvv=" + cv2;

    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'processCallBack',
        contentType: content_type_text,
        dataType: 'jsonp',
        data: ykVars,
        success: function (data) {

            if (data.response.status == "success") {

                ll("#cc_attempts").val(1);

                if ((ll.inArray(data.response.payment_method_type,
                        payment_methods))
                        && (typeof data.response.fields.redirect_url != "undefined")
                        && (data.response.fields.redirect_url.length > 0)) {
                    window.location = data.response.fields.redirect_url;
                } else {
                    var order_id = data.response.fields.order_id;
                    var customer_id = data.response.fields.customer_id;
                    var order_total = data.response.fields.order_total;
                    var gateway_desc = data.response.fields.gatewayDesc;
                    var is_upsell = data.response.is_upsell;
                    var upsell_position = data.response.upsell_position;
                    var package_order_id = data.response.fields.package_order_id;
                    var package_customer_id = data.response.fields.package_customer_id;
                    var package_number = data.response.fields.package_number;
                    var package_gateway = data.response.fields.package_gateway;

                    if (is_upsell == 1
                            && (upsell_position == 1 || upsell_position == 3)) {
                        window.location = "upsell.php?ykPage=1&gateway_desc="
                                + gateway_desc
                                + "&order_id="
                                + order_id
                                + "&customer_id="
                                + customer_id
                                + "&order_total="
                                + order_total
                                + "&package_order_id="
                                + package_order_id
                                + "&package_customer_id="
                                + package_customer_id
                                + "&package_number="
                                + package_number
                                + "&package_gateway="
                                + package_gateway
                                + "&boleto_url=" + ykVars;
                    } else {
                        window.location = "confirm.php?gateway_desc="
                                + gateway_desc + "&order_id="
                                + order_id + "&customer_id="
                                + customer_id + "&order_total="
                                + order_total + "&package_order_id="
                                + package_order_id
                                + "&package_customer_id="
                                + package_customer_id
                                + "&package_number=" + package_number
                                + "&package_gateway=" + package_gateway
                                + "&boleto_url=" + ykVars;
                    }
                }
            } else {
                alert(data.response.fields.error);
                lockButton('submitInstallment', 2);
            }
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('submitInstallment', 2);
        }
    });
}

function showBoleto() {

    if (!lockButton('showBoleto', 1)) {
        return false;
    }

    ll("#credit-card-form-main").hide();
    ll("#boleto-form").show();
    ll("#creditCard-btn").removeClass("checked");
    ll("#boleto-btn").addClass("checked");
    ll("#pay_plan_code").val(0);
    ll("#payment_method_type").val(16);
    var position = ll("#boleto_drop_down").val();
    if (position == 0) {
        position = ll("#straight_sale_pos").val();
    }

    var package_protection_id = ll("#package_protection_id").val();
    var offer_id = ll("#offer_id").val();
    var url = application_url + "/campaigns/special_upsells.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'specialCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "offer_id=" + offer_id + "&special_position=" + position
                + "&package_protection_id=" + package_protection_id
                + "&type=2",
        success: function (data) {
            ll("#straight_sale_pos").val(position);
            ll("#product_id").val(data.settings.product_id);
            ll("#shipping_id").val(data.settings.shipping_id);
            ll("#offer_total").val(data.settings.offer_total);
            ll("span[rel='product']").html(data.settings.product_price);
            ll("span[rel='shipping']").html(
                    data.settings.shipping_price);
            ll("span[rel='total']").html(
                    data.settings.offer_display_total);
            ll("#package-cost").html(data.settings.offer_display_total);
            ll("#package-retail").html(data.settings.retail_price);
            ll("#package-savings").html(data.settings.you_save_price);
            if (position == 0) {
                ll("#promo_savings_display").show();
                ll("#promo_code_display").show();
            } else {
                ll("#promo_savings_display").hide();
                ll("#promo_code_display").hide();
            }
            lockButton('showBoleto', 2);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('showBoleto', 2);
        }
    });
}

function showInstallment() {
    // possible race condition if they click the showBoleto button and showInstallment in quick sequence? - JC 2016-10-10
    if (!lockButton('showInstallment', 1)) {
        return false;
    }

    ll("#boleto-form").hide();
    ll("#credit-card-form-main").show();
    ll("#boleto-btn").removeClass("checked");
    ll("#creditCard-btn").addClass("checked");
    ll("#pay_plan_code").val(1);
    ll("#payment_method_type").val(1);
    var position = ll("#boleto_drop_down").val();
    if (position == 0) {
        position = ll("#straight_sale_pos").val();
    }
    var package_protection_id = ll("#package_protection_id").val();
    var offer_id = ll("#offer_id").val();
    var url = application_url + "/campaigns/special_upsells.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'specialCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "offer_id=" + offer_id + "&special_position=" + position
                + "&package_protection_id=" + package_protection_id
                + "&type=1",
        success: function (data) {
            ll("#straight_sale_pos").val(position);
            ll("#product_id").val(data.settings.product_id);
            ll("#shipping_id").val(data.settings.shipping_id);
            ll("#offer_total").val(data.settings.offer_total);
            ll("span[rel='product']").html(data.settings.product_price);
            ll("span[rel='shipping']").html(
                    data.settings.shipping_price);
            ll("span[rel='total']").html(
                    data.settings.offer_display_total);
            ll("#package-cost").html(data.settings.per_product_price);
            ll("#package-retail").html(data.settings.per_retail_price);
            ll("#package-savings").html(data.settings.per_savings_price);
            if (position == 0) {
                ll("#promo_savings_display").show();
                ll("#promo_code_display").show();
            } else {
                ll("#promo_savings_display").hide();
                ll("#promo_code_display").hide();
            }
            lockButton('showInstallment', 2);
        },
        error: function (e) {
            alert("Network Error Try again Later");
            lockButton('showInstallment', 2);
        }
    });
}

function updateBoleto() {
    // class="price-form-left pull-left bottleImage1"
    // bottleImage3, bottleImage6, bottleImage9, bottleImage12
    // ll("#product-installment-details").show();
    // ll("#boletoPayFieldsWrapper").show();
    var position = ll("#boleto_drop_down").val();

    var package_protection_id = ll("#package_protection_id").val();
    var offer_id = ll("#offer_id").val();
    var url = application_url + "/campaigns/special_upsells.php?callback=?";
    ll.ajax({
        type: 'POST',
        url: url,
        async: false,
        jsonpCallback: 'specialCallBack',
        contentType: "application/json",
        dataType: 'jsonp',
        data: "offer_id=" + offer_id + "&special_position=" + position
                + "&package_protection_id=" + package_protection_id,
        success: function (data) {
            ll("#straight_sale_pos").val(position);
            ll("#product_id").val(data.settings.product_id);
            ll("#shipping_id").val(data.settings.shipping_id);
            ll("#offer_total").val(data.settings.offer_total);
            ll("span[rel='product']").html(data.settings.product_price);
            ll("span[rel='shipping']").html(data.settings.shipping_price);
            ll("span[rel='total']").html(data.settings.offer_display_total);
            if (position == 0) {
                ll("#promo_savings_display").show();
                ll("#promo_code_display").show();
            } else {
                ll("#promo_savings_display").hide();
                ll("#promo_code_display").hide();
            }
        },
        error: function (e) {
            alert("Network Error Try again Later");

        }
    });

}

function getQueryString() {
    return window.location.href.slice(window.location.href.indexOf('?') + 1);
}

function noThanksUpsell() {
    var yKvars = getQueryString();
    window.location = "confirm.php?" + yKvars;
}

function toggleShippingInfo() {
    ll("#hide_shipping_info").toggle();
}

ll(document).ready(function () {
    ll("#email_optin").change(function () {
        if (!ll("#email_optin").is(':checked')) {
            ll("#email_optin").val("off");
        } else if (ll("#email_optin").is(':checked')) {
            ll("#email_optin").val("on");
        }
    });
});

function showPopup() {

    ll("#popup").show();
}

function ykHook(hook, data) {
    ykdebug && console.log("ykHook running", hook, data);
    hook = window['yk' + hook];
    if (typeof hook == 'function') {
        data = data || {};
        return hook();
    } else {
        ykdebug && console.log("ykHook not found");
    }
}

//TODO: args/params (inputs/form?, )
/*
 * 
 * @param {type} page
 * @param {type} form
 * @param {type} callbacks
 *  onPreValidate
 *  onValidate[Pre|Post](Success|Error)
 *  onPreSubmit
 *  onSubmit[Pre|Post](Success|Error)
 * @returns {undefined}
 */
function submitStepN(page, form, callbacks, s) {

    if (!lockButton('submitStepN', 1)) {
        return false;
    }

    var doExtendedDebug = false;
    if (document.location.pathname.indexOf('review.php') > -1 && document.location.href.indexOf('doExtendedDebug') > -1) {
        doExtendedDebug = true;
    }
    if (doExtendedDebug) {
        alert('submitting page ' + page);
    }
    var scope = (typeof s == 'object' && s) ? s : {};
    if (!callbacks || typeof callbacks != 'object') {
        callbacks = {};
    }
    ykHook('SubmitBegin');
    scope.internal = internal = 1; //TODO: wtf does this even mean?
    scope.form = form || ll('#stepn_form');
    scope.elSubmitLink = ll('#submit-link', scope.form);
    scope.offer_id = ll('#offer_id', scope.form).val();

    if (doExtendedDebug) {
        alert('after set scope vars');
    }
    // undo attempted submission
    var submitAbort = function (msg) {
        if (msg) {
            alert(msg);
        }
        ll('#submit-link', scope.form).replaceWith(scope.elSubmitLink);
        lockButton('submitStepN', 2);
        ykHook('SubmitEnd');
    };

    if (page) {
        scope.page = page;
    } else {
        console.error("missing page arg to submitStepN", scope.page);
        submitAbort();
        return;
    }

    var ajaxDefaults = {
        async: false,
        jsonp: false,
        jsonpCallback: 'processCallBack',
        dataType: 'jsonp',
    };

    if (doExtendedDebug) {
        alert('after set scope vars');
    }

    // Validate
    if (typeof callbacks.onPreValidate == 'function') {
        callbacks.onPreValidate(scope);
        if (doExtendedDebug) {
            alert('did pre validate');
        }
    }
    if (doExtendedDebug) {
        alert('prepping ajax call');
    }
    ll.ajax(ll.extend({}, ajaxDefaults, {
        url: application_url + "/campaigns/languages.php",
        data: {step: scope.page, offer_id: scope.offer_id, country: null},
        success: function (data) {
            if (doExtendedDebug) {
                alert('languages success, swap button');
            }
            scope.data = data;
            // Submit
            if (typeof callbacks.onPreSubmit == 'function') {
                callbacks.onPreSubmit(scope);
            }
            scope.elSubmitLink.replaceWith('<div class="rush-order" id="rush_order_btn1"><span id="process">' + scope.data.processing_css + '</span></div>');
            if (doExtendedDebug) {
                alert('doing validator');
            }
            if (form_validator(scope.data.language, scope.data.valid_fields, scope.data.valid_field_names)) {
                if (doExtendedDebug) {
                    alert('validator good.');
                }
                var inputs = scope.form.serializeArray();
                if (!ll('#step', form).length) {
                    inputs.push({name: 'step', value: scope.page});
                }
                ykdebug && console.log('serialized inputs', inputs);
                if (doExtendedDebug) {
                    alert('pre ykprocessor');
                }
                ll.ajax(ll.extend({}, ajaxDefaults, {
                    url: application_url + "/campaigns/ykprocesser.php",
                    data: inputs,
                    success: function (data) {
                        if (doExtendedDebug) {
                            alert('got response from ykprocessor');
                        }
                        scope.data = data;
                        scope.ykVars = "";
                        if (typeof callbacks.onSubmitPreSuccess == 'function') {
                            callbacks.onSubmitPreSuccess(scope);
                        }
                        if (scope.data.response.status == "success") {
                            if (doExtendedDebug) {
                                alert('processed successfully');
                            }
                            if (typeof callbacks.onSubmitPostSuccess == 'function') {
                                callbacks.onSubmitPostSuccess(scope);
                            }
                            ll.each(inputs, function (i, field) {
                                scope.ykVars += "&" + field.name + "=" + encodeURIComponent(field.value);
                            });
                            //TODO: prospectId should be done in callback
                            if (doExtendedDebug) {
                                alert('sending to next page');
                            }
                            window.location = scope.nextPage + (scope.ykVars ? "?" + scope.ykVars : '');
                        } else {
                            submitAbort("Error: " + scope.data.response.fields.error);
                        }
                    },
                    error: function (e) {
                        if (doExtendedDebug) {
                            alert('ykprocessor call failed: network error');
                        }
                        submitAbort("Network Error Try again Later");
                    }
                }));
            } else {
                if (doExtendedDebug) {
                    alert('validation failed');
                }
                submitAbort();
            }
        },
        error: function (e) {
            if (doExtendedDebug) {
                alert('languages call failed.');
            }
            submitAbort("Network Error Try again Later");
        }
    }));
}

function submitFivestepQualify() {
    ll('.exit_pop_header').hide();
    submitStepN(18, null, null, {nextPage: 'review.php'});
    return false;
}

function submitFivestepReview() {
    //TODO
    var callbacks = {};

    callbacks.onSubmitPreSuccess = function (scope) {
        if (ll("#package_protection_id").length > 0 && ll("#package_protection_id").is(':checked') == true) {
            scope.ykVars += "&package_protection_id=" + ll("#package_protection_id").val();
        }
    };

    submitStepN(19, null, callbacks, {nextPage: 'shipping.php'});
    return false;
}

function submitFivestepShipping() {
    var callbacks = {};

    callbacks.onSubmitPreSuccess = function (scope) {
        if (scope.ykVars) {
            scope.ykVars += "&";
        }
        scope.ykVars += "prospectId=" + scope.data.response.prospect_id;
    };

    submitStepN(20, null, callbacks, {nextPage: 'order.php'});
    return false;
}

//TODO: finish?
function submitFivestepOrder() {
    var callbacks = {};

    callbacks.onSubmitPreSuccess = function (scope) {
        var cc = encodeURI(encryptData(ll("#cc_number", scope.form).val()));
        var cv2 = encodeURI(encryptData(ll("#cc_cvv", scope.form).val()));
        cc = cc.replace(/\+/g, "%2B");
        cv2 = cv2.replace(/\+/g, "%2B");
        if (scope.ykVars) {
            scope.ykVars += "&";
        }
        scope.ykVars += "cc_number=" + cc + "&cc_cvv=" + cv2;

        if ((ll("#package_protection_id").length > 0) && (ll("#package_protection_id").is(':checked') == true)) {
            scope.ykVars += "&package_protection_id=" + ll("#package_protection_id").val();
        }

        //TODO: do after if (data.response.status == "success")
        var response = data.response;
        var fields = response.fields;
        if (scope.ykVars) {
            scope.ykVars += "&";
        }
        scope.ykVars += "gateway_desc=" + fields.gatewayDesc
                + "&order_id=" + fields.order_id
                + "&customer_id=" + fields.customer_id
                + "&order_total=" + fields.order_total
                + "&package_order_id=" + fields.package_order_id
                + "&package_customer_id=" + fields.package_customer_id
                + "&package_number=" + fields.package_number
                + "&package_gateway=" + fields.package_gateway;
        if (response.is_upsell == 1 && (response.upsell_position == 1 || response.upsell_position == 3)) {
            scope.nextPage = "upsell.php";
            scope.ykVars += "&ykPage=1";
        } else {
            scope.nextPage = "confirm.php";
        }
    };

    callbacks.onPreValidate = function (scope) {
        //Just count attempts for now
        var elAttempts = ll("#cc_attempts", scope.form);
        var attempts = parseInt(elAttempts.val()) + 1;
        elAttempts.val(attempts);
        //TODO: The reference code in submitStep2Success doesn't do anything?
        /*
         if (attempts++ >= 5) {
         scope.submitAbort(scope.data.decline_message);
         }
         */
    };

    //TODO: value for next?
    //TODO: value for form?
    submitStepN(21, null, callbacks, {nextPage: ''});
    return false;
}

/**
 * lockButton - lock click functions so that we don't have it firing things multiple times
 *  - this should prevent issues with the async calls
 * @param {str} buttonId = selector for the element they're locking (so we can identify each separately)
 * @param {int} lock 1 = request lock or check for lock, 2 (or not 1) = unlock (finished what it was doing)
 * @returns {Boolean} - true = continue (successfully created lock or unlocked), false = prevent execution (already locked, don't re-run)
 */
function lockButton(buttonId, lock) {
    if (!buttonId) {
        // nothing to do, tell them to just keep going
        return true;
    }
    lock = !!lock ? lock : 2; // if not provided, assume they want to unlock (that way it won't break buttons, just in case)
    if (typeof window.buttonsRunning == 'undefined') {
        window.buttonsRunning = {};
    }
    if (lock === 1) {
        if (window.buttonsRunning[buttonId]) {
            // method was requested, but was already locked
            console.log('Already Running ' + buttonId);
            return false;
        }
        // not locked, lock it and tell them to continue execution
        window.buttonsRunning[buttonId] = true;
        return true;
    }
    // requested unlock, unlock and keep going
    window.buttonsRunning[buttonId] = false;
    return true;
}

/**
 * replacement code for the straight sale mess.  hopefully a bit more straight forward (pun intended)
 * assumes several things are populated by the order replacements stuff
 * also assumes some hidden fields as children somewhere nested under clicked item (tile)
 * @param {type} obj - the calling clicked object (should be 'this')
 */
function selectStraightProduct(tile) {

    ll('.straight_prod_tile').removeClass('selected');
    ll('.straight_prod_select').html(ll('#straight_select_text').val());
    ll(tile).find('.straight_prod_select').html(ll('#straight_selected_text').val());

    ll(tile).addClass('selected');
    var prod = tile.querySelector('.ss_prod_id').value;
    var ship = tile.querySelector('.ss_ship_id').value;
    // in case we for whatever reason had a bunch of product_ids jammed in there (ugh)
    var prods = document.querySelectorAll('input[name=product_id]');
    for(var i = 0; i < prods.length; i++) {
        prods[i].value = prod;
    }
    var ships = document.querySelectorAll('input[name=shipping_id]');
    for(var i = 0; i < ships.length; i++) {
        ships[i].value = ship;
    }

    var prod_price = parseFloat(tile.querySelector('.ss_prod_price').value);
    var ship_price = parseFloat(tile.querySelector('.ss_ship_price').value);
    var ship_promo = parseFloat(tile.querySelector('.ss_ship_promo').value);
    var trial_days = parseInt(tile.querySelector('.ss_trial_days').value);
    var trial_bill_days = parseInt(tile.querySelector('.ss_billing_trial_days').value);

    // drop in the new prices for the overall 'selected' price fields so the package check mark will calculate correctly
    document.querySelector('#selected_prod_price').value = prod_price;
    document.querySelector('#selected_ship_price').value = ship_price;
    document.querySelector('#selected_promo_price').value = ship_promo;

    var package = 0;
    var pack = document.querySelector('#package_protection_id');
    if(typeof(pack) != null && parseInt(pack.value) >= 0) {
        var package = (ll("#package_protection_id").is(':checked') == true) ? parseFloat(document.querySelector('#pack_price').value) : 0;
    }

    // might have to add product 'name' switcher? (for now just using brand name)
    ll('#product_price').html(cleanprice(prod_price));
    ll('.product_price').html(cleanprice(prod_price));
    ll('#prices').html(cleanprice(ship_price));
    ll('#protect_price').html(cleanprice(package));
    ll('.price_amount_shipping_discount').html('- ' + cleanprice(ship_promo));
    ll('.trial_days').html(trial_days);
    ll('.billing_trial_days').html(trial_bill_days);

    var totalPrice = cleanprice((prod_price + ship_price + package) - ship_promo);
    document.querySelector('#offer_total_display').innerHTML = totalPrice;
    // in the case the price is in the terms, etc.
    var termsPrices = document.querySelectorAll('.shipping_change');
    for(var i = 0; i < termsPrices.length; i++) {
        termsPrices[i].innerHTML = totalPrice;
    }

}

/**
 * clean up / format price with price symbol / correct separator
 * @param {type} price
 * @returns pretty price text (with symbol)
 */
function cleanprice(price) {
    // may need to put in the thousands separator at some point here (id curr_thousand)
    var curr_pos = document.querySelector('#curr_pos').value;
    var symbol = document.querySelector('#curr_symbol').value;
    var places = parseInt(document.querySelector('#curr_places').value);
    var showprice = price.toFixed(places);

    showprice = showprice.replace(/\./g,document.querySelector('#curr_sep').value);
    showprice = showprice.replace(/\B(?=(\d{3})+(?!\d))/g, document.querySelector('#curr_thousand').value);
    var showprice = ((curr_pos == 0) ? symbol : '') + showprice + ((curr_pos == 1) ? symbol : '');

    return showprice;

}

/**
 *  Attempt to replace goofy server calls for the package protection checky thing
 */
function doPackageClean() {

    var prod_price = parseFloat(document.querySelector('#selected_prod_price').value);
    var ship_price = parseFloat(document.querySelector('#selected_ship_price').value);
    var ship_promo = parseFloat(document.querySelector('#selected_promo_price').value);
    var package =  (ll("#package_protection_id").is(':checked') == true) ? parseFloat(document.querySelector('#pack_price').value) : 0;

    document.querySelector('#protect_price').innerHTML = cleanprice(package);

    var totalPrice = cleanprice((prod_price + ship_price + package) - ship_promo);
    document.querySelector('#offer_total_display').innerHTML = totalPrice;
    var termsPrices = document.querySelectorAll('.shipping_change');
    for(var i = 0; i < termsPrices.length; i++) {
        termsPrices[i].innerHTML = totalPrice;
    }

}