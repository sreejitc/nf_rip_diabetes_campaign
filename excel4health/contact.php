


<!DOCTYPE html>
<html>
<head>
<script type='text/javascript' src="js/master2.js"></script>
  

<script type='text/javascript' src="js/master_css.js"></script> 
<link rel="stylesheet" type="text/css" href='css/master.css'>
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

 
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0"/>
<link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Barlow+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Barlow:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Open+Sans+Condensed:300,300i,700|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Oswald:200,300,400,500,600,700|Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i|Roboto+Slab:100,200,300,400,500,600,700,800,900&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Fira+Sans+Extra+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Fira+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"><style type="text/css">
#termsHeader, #wrapper {
    position: relative;
    padding: 0;
    width: 100%;
	max-width: 1000px;
	margin: 0 auto;
}
#protectInnnerWrapper, #termsWrapper {
    background: 0 0;
    color: #000;
    margin: 0 auto
}
body {
    background: #FFF;
    margin: 0;
    padding: 0;
    color: #000
}
#wrapper {
    margin: 0 auto;
    top: 0
}
#termsHeader {
    height: 0;
    margin: 0 auto
}
#termsWrapper {
    width: 100%;
}
#termsPadder {
    padding: 1em 2%;
    text-align: left
}
h3 {
    font: 2em;
    text-transform: uppercase
}
#termsFooter, .footer_logos {
    display: none
}
#termsFooterPadder {
    padding: 10px 20px
}
#countries td {
    border: 1px solid #000;
    padding: 6px
}
#protectWrapper {
    width: 1000px;
    margin: 0 auto;
    padding: 0
}
#protectInnnerWrapper {

}
#protectPadder {
    padding: 0;
    text-align: left
}
.packageProtectInfo {
    width: 910px;
    height: 300px;
    margin: 0 auto;
    padding: 0;
    border: 2px dashed #DEA529;
    background: url(images/packBg.jpg) center no-repeat #2a2a2a;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px
}
.packageProtectInfo ul li {
    list-style: circle;

    color: #fff;
    font-weight: 700;
    letter-spacing: 1px;
    text-shadow: #000 1px 1px
}
.congrats {
    color: #FFBD2F;

    font-weight: 700;
    text-shadow: #000 1px 1px
}
.protectLink, .protectLink a, .protectLink a:active, .protectLink a:link, .protectLink a:visited {

    color: #FFBD2F
}
.protectLink a:hover {

    color: #ccc
}
#protectFooter {
    width: 1000px;
    margin: 0 auto
}
#protectFooterPadder {
    padding: 20px
}
.contactBody {
    background: #fff
}
#contactWrapper {
    width: 100%;
    background: #fff;
    margin: 0 auto 5%;
    
}
#contactPadder {
    padding: 5%;
    text-align: left
}
#contactTitle {
    font-size: 2em;
    font-weight: 700
}
#contactPhoneText {
    font-size: 1.3em;
    font-weight: 700;
    padding: 3% 0 2%
}
#contactLeft, #contactRight {
    width: 50%;
    float: left
}
.contactCountry {
    font-size: 1.1em;
    font-weight: 700;
    padding: 0 0 2%
}
.contactCity {
    padding: 0 0 0 5%
}
.contactNumber {
    padding: 0 0 2% 5%
}
#contactFooter {
    color: #000;
    text-align: center
}
.clearBoth {
    clear: both
}
@media only screen and (max-width:768px) {
	#termsPadder {
    	padding: 1em 5%;
	}
}
</style>
<style>
html, body{
	font-family:  'Open Sans','Source Sans Pro', 'Roboto', sans-serif;
	font-weight: 400;
	font-size: 16px;
	line-height: normal;
}
</style>
<link href="images/carerenew.png" rel="shortcut icon" type="image/x-icon">
</head>
<body class="contactBody">
<div id="pageWrapper" align="center">
<div id="wrapper">
  <div id="contactWrapper">
    <div id="contactHeader"></div>

    <div id="contactPadder">
      <div class="contactInfo">
        <div id="contactTitle">
          <div> Contact Us</div>
        </div>
        <div id="contactText">
          <div> <p>We take great pride in our support team, all of our agents have gone through many training procedures to ensure they can give every customer 100% satisfaction with all of their needs. For any questions or concerns with your order - please contact our support team.</p><p>Open 24 Hours a Day, 7 Days a Week, 365 Days a Year</p>							<p class="email"><strong>E-mail:</strong> support@excel4health.com</p></div>
        </div>
        <div id="contactPhoneText">
          <div> Phone Support</div>
        </div>
        <div id="contactLeft">            <div class="contactCountry">United States</div>            <div class="contactNumber">844-857-0525</div>            <div class="contactNumber">833-664-0611</div>        </div>
        <div class="clearBoth"></div>
      </div>
    </div>


  </div>
</div>
 
</html>
	
