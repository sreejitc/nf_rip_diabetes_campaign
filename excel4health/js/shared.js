var $j = jQuery.noConflict();

(function () {

  if (document.getElementById('pop-up')) {
    document.getElementById('pop-up').style.display = 'none';
  }

  if (document.getElementById('pop-up2')) {
    document.getElementById('pop-up2').style.display = 'none';
  }

  if (document.getElementById('pop-up3')) {
    document.getElementById('pop-up3').style.display = 'none';
  }

  window.addEventListener('popstate', function (event) {
    history.pushState(null, null, window.location);
  });

  window.history.forward(-1);
})();

function showLoadingPopup() {
  $j('body').css('overflow', 'hidden');
  document.getElementById('loading').style.display = 'block';
  //$j('#loading').show();
}