
<!DOCTYPE html>
<html>
<head>
<script type='text/javascript' src="js/master2.js"></script>

 

<script type='text/javascript' src="js/master_css.js"></script> 
<link rel="stylesheet" type="text/css" href='css/master.css'>
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0"/>
<link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Barlow+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Barlow:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Open+Sans+Condensed:300,300i,700|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Oswald:200,300,400,500,600,700|Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i|Roboto+Slab:100,200,300,400,500,600,700,800,900&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Fira+Sans+Extra+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Fira+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"><style type="text/css">
#termsHeader, #wrapper {
    position: relative;
    padding: 0;
    width: 100%;
	max-width: 1000px;
	margin: 0 auto;
}
#protectInnnerWrapper, #termsWrapper {
    background: 0 0;
    color: #000;
    margin: 0 auto
}
body {
    background: #FFF;
    margin: 0;
    padding: 0;
    color: #000
}
#wrapper {
    margin: 0 auto;
    top: 0
}
#termsHeader {
    height: 0;
    margin: 0 auto
}
#termsWrapper {
    width: 100%;
}
#termsPadder {
    padding: 1em 2%;
    text-align: left
}
h3 {
    font: 2em;
    text-transform: uppercase
}
#termsFooter, .footer_logos {
    display: none
}
#termsFooterPadder {
    padding: 10px 20px
}
#countries td {
    border: 1px solid #000;
    padding: 6px
}
#protectWrapper {
    width: 1000px;
    margin: 0 auto;
    padding: 0
}
#protectInnnerWrapper {

}
#protectPadder {
    padding: 0;
    text-align: left
}
.packageProtectInfo {
    width: 910px;
    height: 300px;
    margin: 0 auto;
    padding: 0;
    border: 2px dashed #DEA529;
    background: url(images/packBg.jpg) center no-repeat #2a2a2a;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px
}
.packageProtectInfo ul li {
    list-style: circle;

    color: #fff;
    font-weight: 700;
    letter-spacing: 1px;
    text-shadow: #000 1px 1px
}
.congrats {
    color: #FFBD2F;

    font-weight: 700;
    text-shadow: #000 1px 1px
}
.protectLink, .protectLink a, .protectLink a:active, .protectLink a:link, .protectLink a:visited {

    color: #FFBD2F
}
.protectLink a:hover {

    color: #ccc
}
#protectFooter {
    width: 1000px;
    margin: 0 auto
}
#protectFooterPadder {
    padding: 20px
}
.contactBody {
    background: #fff
}
#contactWrapper {
    width: 100%;
    background: #fff;
    margin: 0 auto 5%;
    
}
#contactPadder {
    padding: 5%;
    text-align: left
}
#contactTitle {
    font-size: 2em;
    font-weight: 700
}
#contactPhoneText {
    font-size: 1.3em;
    font-weight: 700;
    padding: 3% 0 2%
}
#contactLeft, #contactRight {
    width: 50%;
    float: left
}
.contactCountry {
    font-size: 1.1em;
    font-weight: 700;
    padding: 0 0 2%
}
.contactCity {
    padding: 0 0 0 5%
}
.contactNumber {
    padding: 0 0 2% 5%
}
#contactFooter {
    color: #000;
    text-align: center
}
.clearBoth {
    clear: both
}
@media only screen and (max-width:768px) {
	#termsPadder {
    	padding: 1em 5%;
	}
}
</style>
<style>
html, body{
	font-family:  'Open Sans','Source Sans Pro', 'Roboto', sans-serif;
	font-weight: 400;
	font-size: 16px;
	line-height: normal;
}
</style>
<link href="images/carerenew.png" rel="shortcut icon" type="image/x-icon">
</head>

<body>
<div id="pageWrapper" align="center">
  <div id="wrapper">
    <div id="termsWrapper">
    <div id="termsPadder">
        <div> <h3>TERMS AND CONDITIONS OF USE </h3> <br> CAREFULLY READ AND UNDERSTAND THESE TERMS BEFORE ORDERING ANY PRODUCT THROUGH THIS WEBSITE<br><br>ATTENTION: This is a legal agreement (the "Agreement") between You, the individual, company or organization ("you," "your," or "Customer") and Excel 4 Health ("we," "our" or "Company"). By ordering, accessing, using or purchasing Excel 4 Health Cluco-D ("Product") through this website or related websites (collectively the "Website"), you are agreeing to be bound by, and are becoming a party to, this Agreement. We may at our sole and absolute discretion change, add, modify, or delete portions of this Agreement at any time without notice. It is your sole responsibility to review this Agreement for changes prior to use of the Website or purchase of the Product.<br><br>ANY CLAIM OR DISPUTE BETWEEN YOU AND THE COMPANY THAT ARISES OUT OF OR RELATES TO THIS AGREEMENT SHALL BE RESOLVED BY BINDING ARBITRATION PURSUANT TO THE STANDARD TERMS SET OUT BELOW. YOU AGREE TO WAIVE ANY RIGHT TO TRIAL BY JURY IN ANY DISPUTE WITH THE COMPANY. YOU FURTHER AGREE THAT YOU WILL NOT PARTICIPATE IN ANY CLASS ARBITRATION OR CLASS ACTION LAWSUIT, AND THAT ANY CLAIM BETWEEN YOU AND THE COMPANY MAY FAIRLY AND APPROPRIATELY BE RESOLVED IN AN INDIVIDUAL ARBITRATION.<br><br>IT IS STRONGLY RECOMMENDED THAT YOU REVIEW THIS DOCUMENT IN ITS ENTIRETY BEFORE ACCESSING, USING OR BUYING ANY PRODUCT THROUGH THE WEBSITE<br><br>1. ORDER TERMS AND CONDITIONS<br><br>2. CONDITIONS OF AGREEMENT<br><br>These terms and conditions apply to ALL transactions made on or through this Website. This Agreement is intended to be governed by the Electronic Signatures in Global and National Commerce Act. You manifest your agreement to the terms and conditions in this document by any act demonstrating your assent thereto, including clicking any button containing the words "I agree" or similar syntax, or by merely accessing the Website, whether you have read these terms or not. It is suggested that you print this form for your personal records.<br><br>3. GENERAL<br><br>By placing an order with us, you will be deemed to have read, understood, and agreed to these Terms and Conditions of Use (collectively, "Terms"). If you do not agree to be bound by these Terms, you may not access or use the Website, or purchase any Product(s) through the Website. By accessing, using or ordering Product(s) through the Website, you affirm that you have read this Agreement and understand, agree and consent to all Terms contained herein.<br><br>4. TEMPORARY PRICE REDUCTION<br><br>For your convenience and benefit, Excel 4 Health Cluco-D may temporarily reduce the price of your order for promotional purposes, or to ensure that your purchase order transaction is capable of being processed by your credit card company or processor. If Excel 4 Health Cluco-D reduces the price of the Product as described herein, you will be billed at the reduced price until the promotional period ends or Excel 4 Health Cluco-D is assured of payment by your credit card company or its processor, after which time the Product price will be restored to its usual price, without prior notice to you. If your order is processed at the reduced price a new billing cycle will begin from the date of the new payment processing. Any temporary reduction in the price will not alter or enlarge the 0 day cancellation terms referenced above.<br><br>5. ORDER PERIOD<br><br>5.1 The prices for the products are as follows: $199.95(+ free S&amp;H) for the 5 bottle package ($39.99 bottle); $179.97 (+ free S&amp;H) for the 3 bottle package ($59.99 bottle); $139.98 (+ free S&amp;H) for the 2 bottle package ($69.99 bottle).<br><br>5.2 You authorize us to initiate a charge to your credit card as indicated upon your purchase.<br><br>5.3 (Cont) Excel 4 Health Cluco-D is sold as a program with the price and product being charged and delivered one the initial sale, and then again monthly (every 30 days after the initial purchase). Depending on the selected number of products upon initial purchase, the price per installment will vary. For 2 bottle, each installment is $139.98 . For 3 bottles, each installment is $179.97 and for 5 bottles, each installment is $199.95. The program begins when the customer signs up, and continues indefinitely until the customer shall chose to cancel. The secondary product is optionally sold as a single bottle add on purchase with the initial order only.<br><br>5.4 Additional Opt-in Product: Excel 4 Health Cleanse, if purchased, you are ordering a 30 day supply and will be charged $60.00 (+ free S&amp;H) for the product you receive. This purchase is backed by a 100% Money Back Guarantee. If at some point you choose to cancel this purchase, call customer service at 844-946-0263 anytime.<br><br>If you are unsatisfied with Excel 4 Health Cluco-D for any reason, simply cancel your order by contacting our Customer Care department at 844-946-0263 or please visit us online at excel4health.com/support/.<br><br>6. REFUND POLICY (Applicable to all Products Ordered from Company)<br><br>To return a Product for an exchange due to shipping damage you will need to obtain a Return Merchandize Authorization (“RMA”) number by contacting the Customer Care Department at 844-946-0263. An RMA number can ONLY be obtained by contacting the Customer Care Department by phone.<br><br>Please Note:<br><br>We cannot process or exchange Product marked "Return to Sender." If you “Return to Sender” or fail to obtain an RMA, we will be unable to process or verify your return.<br><br>To ensure that your account is correctly noted, you must send back Product returns to the address provided below along with your RMA number. The RMA number must be clearly written on the package that you are sending back. Our shipping department is NOT allowed to accept any packages without an RMA number.<br><br>Returned Products must be sent to the following address:<br><br>Excel 4 Health Cluco-D<br><br>PO Box 0538, Toa Payoh Central Post Office, Singapore 913119<br><br>We are not responsible for lost or stolen items. We recommend all returned items to be sent using some type of third party delivery confirmation system to ensure proper delivery.<br><br>7. SHIPPING TERMS<br><br>Products will be shipped to you using one of many mail level services. All packages should arrive within five to seven (5-7) business days. Please note that shipments are not sent out on Saturdays, Sundays, or any Holidays. We do not guarantee arrival dates or times. WE DO NOT REFUND OR CREDIT SHIPPING CHARGES FOR ANY MONTHLY SHIPMENTS OR RETURNS.<br><br>7.1 How to Alter Your Shipping<br><br>Please contact our Customer Care Department available at 844-946-0263 to help you alter your delivery schedule if you need to modify the automatic 0 day delivery schedule.<br><br>8. BILLING ERRORS<br><br>If you believe that you have been billed in error, please notify our Customer Care Department at 844-946-0263 immediately. If we do not hear from you within 30 days after such billing error first appears on any account statement, the billing will be deemed irrevocably accepted by you for all purposes, including resolution of inquiries made by your credit card issuer. You are deemed to have released Excel 4 Health Cluco-D from all liabilities and claims of loss resulting from any error or discrepancy that is not reported to Excel 4 Health Cluco-D within thirty (30) days of its appearance on your credit card account statement.<br><br>9. REPRESENTATIONS; DISCLAIMERS<br><br>It is our Company mission to provide our customers with the finest Products available. We believe in the efficacy of the Products we sell. You understand, however, that the statements on the Website, promotional materials and the Product have not been evaluated by the by any local or international regulatory bodies, and the Product is not intended to diagnose, treat, cure or prevent any disease. The information provided by our Websites or this Company is not a substitute for a face-to-face consultation with your health care professional and should not be construed as individual medical advice. Individual results will vary.<br><br>The Product is intended for use by persons at least 18 years of age. If you are pregnant, nursing or taking any medication, you represent and warrant that you either have consulted, or will consult, with a health care professional before taking the Product, and you will cease immediately taking the Product and will contact a health care professional if you experience any ill effects or unintended side effects of the Product.<br><br>We want you to have the most accurate information concerning the Product. The information we communicate to you about the Product and/or its efficacy is obtained from independent third parties such as educational institutions, scientific and news articles and agencies, nutritional specialists, scientific reports and researchers ("Information Sources"). We do not warrant or represent that Information Sources are not error-free, nor do we warrant any Information Source or the methods that they use to arrive at their conclusions. All Product specifications, performance data and other information on our Websites are for informational and illustrative purposes only, and do not constitute a guarantee or representation that the Product will conform to such specifications or performance data.<br><br>We do not guarantee that you will have any specific or particular result or benefit from the Product, or that your experience will match those of others who use the Product. Individual results will vary from person to person.<br><br>10. YOUR REPRESENTATIONS<br><br>You represent that you are at least 18 years of age and that you will not permit a person under 18 to order, or use, the Product. You represent that the information provided by you when placing your order is up-to-date, materially accurate and 
        sufficient for us to fulfill your order in a timely and efficient manner. You are responsible for maintaining
         and promptly updating your account information with us and keeping such information (and any passwords given to you for the purposes of accessing the Website and/or purchasing Products) secure against unauthorized access. Unless agreed otherwise or required by applicable law, any warranties provided in relation to the Product only extend to you on the understanding that you are a user, and not a reseller, of the Product. You expressly covenant that you shall not re-sell, re-distribute or export any Product that you order from the Website.<br><br>You agree to pay for the Product and any taxes, shipping or handling of Product as such costs are specified by us on the Website when you submit your purchase order. Payment shall be made prior to delivery and by such methods as indicated on the Website (and not by any other means unless we have given our prior consent to such alternative payment methods).<br><br>11. REJECTION, DAMAGE OR LOSS IN TRANSIT<br><br>We shall not be liable and you shall not be entitled to reject Product delivery, except for damage to the Product or any part thereof occurring in transit (where the Product is carried by our own transport or by a carrier on our behalf), and where we are notified of such damage within five (5) business days of your receipt of the Product.<br><br>12. LIABILITY LIMITATION AND ARBITRATION<br><br>LIABILITY LIMITATION: TO THE MAXIMUM EXTENT LEGALLY PERMITTED, WHETHER OR NOT COMPANY WAS AWARE OR ADVSED OF THE POSSIBILITY OF DAMAGES, AND WHETHER OR NOT THE LIMITED REMEDIES PROVIDED HEREIN FAIL OF THEIR ESSENTIAL PURPOSE, OUR AGGREGATE LIABILITY (WHETHER FOR BREACH OF CONTRACT, TORT OR ANY OTHER LEGAL THEORY) SHALL IN NO CIRCUMSTANCES EXCEED THE COST OF THE PRODUCTS YOU ORDERED (LESS ANY REFUNDS). FURTHER, UNDER NO CIRCUMSTANCES SHALL WE BE LIABLE FOR SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES, LOST PROFITS, LOST REVENUE, COST OF COVER, OR EXEMPLARY OR PUNITIVE DAMAGES. THE PRODUCTS ARE SOLD AND DELIVERED TO YOU "AS IS" WITH NO WARRANTY WHATSOEVER. EXCEPT AS EXPRESSLY STATED OTHERWISE IN THIS SECTION, WE MAKE NO EXPRESS WARRANTIES OR REPRESENTATIONS AND WE DISCLAIM ALL IMPLIED WARRANTIES AND REPRESENTATIONS, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.<br><br>ARBITRATION: ANY CONTROVERSY OR CLAIM ARISING OUT OF OR RELATING TO THIS CONTRACT, OR THE BREACH THEREOF, INCLUDING WITHOUT LIMITATION ANY DISPUTE CONCERNING THE CONSTRUCTION, VALIDITY, INTERPRETATION, ENFORCEABILITY OR BREACH OF THE AGREEMENT, SHALL BE EXCLUSIVELY RESOLVED BY BINDING ARBITRATION ADMINISTERED UNDER THE COMMERCIAL ARBITRATION RULES OF THE AMERICAN ARBITRATION ASSOCIATION. THE NUMBER OF ARBITRATORS SHALL BE ONE OR THREE. THE PLACE OF ARBITRATION SHALL BE ST. KITTS. ST. KITTS LAW SHALL APPLY. JUDGMENT ON THE AWARD RENDERED BY THE ARBITRATOR(S) MAY BE ENTERED IN ANY COURT HAVING JURISDICTION THEREOF. PER THE TERMS OF THIS AGREEMENT, THE ARBITRATOR[S] SHALL HAVE NO AUTHORITY TO AWARD PUNITIVE / CONSEQUENTIAL / SPECIAL / INDIRECT DAMAGES.<br><br>THIS AGREEMENT TO ARBITRATE SHALL BE SPECIFICALLY ENFORCEABLE. A PARTY MAY APPLY TO ANY COURT WITH JURISDICTION FOR INTERIM OR CONSERVATORY RELIEF, INCLUDING WITHOUT LIMITATION A PROCEEDING TO COMPEL ARBITRATION IN ST. KITTS.<br><br>THE DEMAND FOR ARBITRATION SHALL BE MADE WITHIN A REASONABLE TIME AFTER THE CLAIM, DISPUTE OR OTHER MATTER IN QUESTION HAS ARISEN, AND IN NO EVENT SHALL IT BE MADE AFTER ONE YEAR FROM WHEN THE AGGRIEVED PARTY KNEW OR SHOULD HAVE KNOWN OF THE CONTROVERSY, CLAIM, DISPUTE OR BREACH.<br><br>13. INDEMNIFICATION<br><br>You agree to defend, indemnify, and hold harmless Excel 4 Health Cluco-D, its officers, directors, shareholders, employees, independent contractors, telecommunication providers, and agents, from and against any and all claims, actions, loss, liabilities, expenses, costs, or demands, including without limitation legal and accounting fees, for all damages directly, indirectly, and/or consequentially resulting or allegedly resulting from your misuse of the Website, or your breach of any of these terms and conditions of this Agreement. We shall promptly notify you by electronic mail of any such claim or suit, and cooperate fully (at your expense) in the defense of such claim or suit. If we do not hear from you promptly, we reserve the right to defend such claim or suit and seek full recompense from you.<br><br>14. NOTICES<br><br>Any notice or other communications arising in relation to this Agreement shall be given by sending an e-mail to the latest email address that one party has notified in writing to the other. In the case of Excel 4 Health Cluco-D, the email address can be found at excel4health.com/support/. In the case of sending notices to you, Excel 4 Health Cluco-D will use the email address you provided to Excel 4 Health Cluco-D when you ordered your Product. If you change your e-mail address, you are required to provide us written notice the change. Such notices or communications (where properly addressed) shall be considered received on the earliest of (i) the email being acknowledged by the recipient as received; (ii) receipt by the sender of an automated message indicating successful delivery or the email having been opened; or (iii) the expiry of forty-eight (48) hours after transmission, provided that the sender has not received notification of unsuccessful transmission.<br><br>15. TERMINATION<br><br>We reserve the right to terminate your access to or use of this Website and/or your subscription to the Product should we believe that you have violated any of the terms of this Agreement or if we believe you have sought, in bad faith, charge backs, credit backs, Product returns, discounts or any other conduct designed to injure, harass or disrupt this Website or the Companys business operations.<br><br>16. FRAUD<br><br>We reserve the right, but undertake no obligation, to actively report and prosecute actual and suspected credit card fraud. We may, in our discretion, require further authorization from you such as a telephone confirmation of your order and other information. We reserve the right to cancel, delay, refuse to ship, or recall from the shipper any order if fraud is suspected. We capture certain information during the order process, including time, date, IP address, and other information that will be used to locate and identify individuals committing fraud. If any Web Site order is suspected to be fraudulent, we reserve the right, but undertake no obligation, to submit all records, with or without a subpoena, to all law enforcement agencies and to the credit card company for fraud investigation. We reserve the right to cooperate with authorities to prosecute offenders to the fullest extent of the law.<br><br>17. SALES TAX<br><br>If you purchase any Products available on our websites, you will be responsible for paying any sales tax indicated on the Web Site.<br><br>18. INTELLECTUAL PROPERTY RIGHTS<br><br>The Website, and all content appearing therein, are the sole and exclusive property of the Excel 4 Health Cluco-D or its licensors. No license or ownership rights in or to any content of the Website are conveyed to you by reason of this Agreement or your purchase of Product. The Website and its content are protected under the laws of copyright and trademark. Unless otherwise permitted by law, you may not copy, republish or transmit any portion of the Website without Excel 4 Health Cluco-Ds prior written consent.<br><br>19. FOREIGN TRANSACTION FEES<br><br>In some instances, billing for your product or shipping fee may originate from outside of your local area and in some occasions, your financial institution may charge a fee for processing this payment. You are responsible for these processing fees assessed by your financial institution per your card holder agreement.<br><br>20. CURRENCY EXCHANGE RATES AND FEES<br><br>By placing your order through this website, you understand that all payments for products and services may be billed in a non local currency. When placing an order from certain countries or using a foreign means of payment, all payments will be subject to any applicable transfer, bank, and currency exchange fees. As currency exchange rates are ever changing, it is the customers responsibility to be aware of the exchange rate and any applicable transfer, bank, and currency exchange fees which may apply for their purchase.<br><br>21. MISCELLANEOUS<br><br>Governing Law. This Agreement and all disputes or issues arising from it shall be governed exclusively by the laws of St. Kitts without regard to choice or conflict of law principles. The sole and exclusive venue for any and all claims or causes of action arising from or related to this Agreement, or that are related in any manner to your purchase or attempted purchase of the Product(s), shall be the arbitration in St. Kitts. YOU SPECIFICALLY WAIVE THE RIGHT TO BRING ANY CLAIM OR CAUSE OF ACTION IN ANY OTHER STATE, COUNTRY, OR REGION.<br><br>Assignment. This Agreement and the rights and liabilities of the parties hereto inure to the benefit of their respective successors and assigns. Excel 4 Health Cluco-D may assign this Agreement to any successor entity. Customer may not assign without the written permission of Company.<br><br>Severability. If for any reason a court of competent jurisdiction or an arbitrator finds any provision of this Agreement, or any portion thereof, to be unenforceable, that provision will be enforced to the maximum extent permissible and the remainder of these Terms and Conditions will continue in full force and effect. The unenforceability of any provision of this Agreement will not otherwise negate or nullify the enforceable terms.<br><br>Attorneys Fees. In the event any Party shall commence any claims, actions, formal legal action, or arbitration to interpret and/or enforce the terms and conditions of this Agreement, or relating in any way to this Agreement, including without limitation asserted breaches of representations and warranties, the prevailing party in any such action or proceeding shall be entitled to recover, in addition to all other available relief, its reasonable attorneys fees and costs incurred in connection therewith, including attorneys fees incurred on appeal.<br><br>No Waiver. No waiver of or by Excel 4 Health Cluco-D shall be deemed a waiver of any subsequent default of the same provision of this Agreement.<br><br>Headings. All headings are solely for the convenience of reference and shall not affect the meaning, construction or effect of this Agreement.<br><br>Complete Agreement. This Agreement constitutes the entire agreement between the parties with respect to your access and use of the Website and your ordering and use of the Product, and supersedes and replaces all prior understandings or agreements, written or oral, regarding such subject matters.<br><br>Modifications. Excel 4 Health Cluco-D reserves the right to change any of the provisions posted herein and you agree to review these terms and conditions each time you visit the Website. Your continued use of the Website following the posting of any changes to these terms and conditions constitutes your acceptance of such changes. Excel 4 Health Cluco-D does not and will not assume any obligation to provide you with notice of any change to this document and you acknowledge and agree to same. Unless accepted by Excel 4 Health Cluco-D in writing, you may not amend these terms and conditions in any way.<br><br>
        </div>
      </div>
    </div>
  </div>
</div>

</html>