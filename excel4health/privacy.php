
<!DOCTYPE html>
<html>
<head>
<script type='text/javascript' src="css/master2.js"></script>
 

<script type='text/javascript' src="js/master_css.js"></script> 
<link rel="stylesheet" type="text/css" href='css/master.css'>
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

 
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0"/>
<link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Barlow+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Barlow:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Open+Sans+Condensed:300,300i,700|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Oswald:200,300,400,500,600,700|Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i|Roboto+Slab:100,200,300,400,500,600,700,800,900&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Fira+Sans+Extra+Condensed:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Fira+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet"><style type="text/css">
#termsHeader, #wrapper {
    position: relative;
    padding: 0;
    width: 100%;
	max-width: 1000px;
	margin: 0 auto;
}
#protectInnnerWrapper, #termsWrapper {
    background: 0 0;
    color: #000;
    margin: 0 auto
}
body {
    background: #FFF;
    margin: 0;
    padding: 0;
    color: #000
}
#wrapper {
    margin: 0 auto;
    top: 0
}
#termsHeader {
    height: 0;
    margin: 0 auto
}
#termsWrapper {
    width: 100%;
}
#termsPadder {
    padding: 1em 2%;
    text-align: left
}
h3 {
    font: 2em;
    text-transform: uppercase
}
#termsFooter, .footer_logos {
    display: none
}
#termsFooterPadder {
    padding: 10px 20px
}
#countries td {
    border: 1px solid #000;
    padding: 6px
}
#protectWrapper {
    width: 1000px;
    margin: 0 auto;
    padding: 0
}
#protectInnnerWrapper {

}
#protectPadder {
    padding: 0;
    text-align: left
}
.packageProtectInfo {
    width: 910px;
    height: 300px;
    margin: 0 auto;
    padding: 0;
    border: 2px dashed #DEA529;
    background: url(images/packBg.jpg) center no-repeat #2a2a2a;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px
}
.packageProtectInfo ul li {
    list-style: circle;

    color: #fff;
    font-weight: 700;
    letter-spacing: 1px;
    text-shadow: #000 1px 1px
}
.congrats {
    color: #FFBD2F;

    font-weight: 700;
    text-shadow: #000 1px 1px
}
.protectLink, .protectLink a, .protectLink a:active, .protectLink a:link, .protectLink a:visited {

    color: #FFBD2F
}
.protectLink a:hover {

    color: #ccc
}
#protectFooter {
    width: 1000px;
    margin: 0 auto
}
#protectFooterPadder {
    padding: 20px
}
.contactBody {
    background: #fff
}
#contactWrapper {
    width: 100%;
    background: #fff;
    margin: 0 auto 5%;
    
}
#contactPadder {
    padding: 5%;
    text-align: left
}
#contactTitle {
    font-size: 2em;
    font-weight: 700
}
#contactPhoneText {
    font-size: 1.3em;
    font-weight: 700;
    padding: 3% 0 2%
}
#contactLeft, #contactRight {
    width: 50%;
    float: left
}
.contactCountry {
    font-size: 1.1em;
    font-weight: 700;
    padding: 0 0 2%
}
.contactCity {
    padding: 0 0 0 5%
}
.contactNumber {
    padding: 0 0 2% 5%
}
#contactFooter {
    color: #000;
    text-align: center
}
.clearBoth {
    clear: both
}
@media only screen and (max-width:768px) {
	#termsPadder {
    	padding: 1em 5%;
	}
}
</style>
<style>
html, body{
	font-family:  'Open Sans','Source Sans Pro', 'Roboto', sans-serif;
	font-weight: 400;
	font-size: 16px;
	line-height: normal;
}
</style>
<link href="images/carerenew.png" rel="shortcut icon" type="image/x-icon">
</head>

<body>
<div id="pageWrapper" align="center">
  <div id="wrapper">
    <div id="termsWrapper">
    <div id="termsPadder">
        <div> <h3>Privacy Policy </h3> <p>PLEASE NOTE: OUR PRIVACY POLICY CHANGES FROM TIME TO TIME AND CHANGES    ARE EFFECTIVE UPON POSTING. PLEASE CHECK BACK FREQUENTLY FOR UPDATES AS    IT IS YOUR SOLE RESPONSIBILITY TO BE AWARE OF CHANGES. Excel 4 Health Cluco-D DOES NOT PROVIDE NOTICES OF CHANGES IN ANY MANNER OTHER THAN BY     POSTING THE CHANGES AT THIS WEB SITE.    IF YOU DO NOT AGREE WITH THE TERMS OF THIS PRIVACY POLICY PLEASE DO     NOT PROVIDE ANY INFORMATION TO Excel 4 Health Cluco-D OR USE ANY OF THE     SERVICES OR PRODUCTS OFFERED OR PROVIDED ON ANY OF THE WEB SITES   REFERRED TO IN THIS PRIVACY POLICY. <br><br>  The information collection, use, and dissemination practices of Excel 4 Health Cluco-D     This policy applies to Excel 4 Health Cluco-D’s collection, use,   storage and disclosure of information by Excel 4 Health Cluco-D on its <br><br>  (a) websites, including all its divisions, subsidiaries and related     companies (collectively, the "Websites"), <br><br>  (b) on various third party websites, and <br><br>  (c) to Excel 4 Health Cluco-Ds other information collection,     including the purchase of customer lists from third parties. Excel 4 Health Cluco-D is not responsible for the information collection or   privacy practices of third web sites or applications. <br><br>  1. Collection of Information. <br><br>  1.1 Survey Information. Excel 4 Health Cluco-D collects information from individuals by various    methods, including, but not limited to, when an individual voluntarily     completes a Excel 4 Health Cluco-D survey, order form, or a registration    page either online or offline, or by means of online or offline     surveys, order forms, or registration pages operated by third parties     (collectively, a "Survey"). (As used herein, "online" means using the     Internet, including the Websites, and related technologies, and     "offline" means by methods other than online, including in person, in     the postal mail, using telephones and cell phones, and other similar     means.) In the Surveys, Excel 4 Health Cluco-D or a third party (a "Third    Party") may ask an individual to provide various information to Excel 4 Health Cluco-D, which may include his or her name, email address,    street address, zip code, telephone numbers (including cell phone     numbers and carriers), birth date, gender, salary range, education and     marital status, occupation, social security number, employment     information, personal and online interests, and such other information     as may be requested from time to time (together, "Survey Information"). Excel 4 Health Cluco-D may also collect information concerning an     individual from another source and uses that information in combination     with information provided from this web site. Completing the Surveys is     completely voluntary, and individuals are under no obligation to provide    Survey Information to Excel 4 Health Cluco-D or Third Party, but an     individual may receive incentives from Excel 4 Health Cluco-D or a Third   Party in exchange for providing Survey Information to Excel 4 Health Cluco-D<br><br>  1.2 Third Party List Information. Excel 4 Health Cluco-D collects information from individuals when an individual provides information through Excel 4 Health Cluco-D websites.  When acquiring this information, Excel 4 Health Cluco-D seeks assurances from the customer that the customer has a right to provide this information to Excel 4 Health Cluco-D.  Excel 4 Health Cluco-D assure customer that customer credit card and financial data will never be sold, transferred, or shared with a third party.<br><br>  1.3 Other Information.    Other occasions when Excel 4 Health Cluco-D obtains information from   individuals include <br><br>  (1) when an individual is making a claim for a prize or seeking to     redeem an incentive offered by Excel 4 Health Cluco-D or by a third   party, <br><br>  (2) when an individual requests assistance through Excel 4 Health Cluco-Ds customer service department, and <br><br>  (3) when an individual voluntarily subscribes to a Excel 4 Health Cluco-D service or newsletter (together, "Other Information"). <br><br>  1.4 Cookies, Web Beacons, and Other Info Collected Using Technology. Excel 4 Health Cluco-D currently uses cookie and web beacon technology     to associate certain Internet-related information about an individual     with information about the individual in our database. Additionally, Excel 4 Health Cluco-D may use other new and evolving sources of   information in the future (together, "Technology Information"). <br><br>  (a) Cookies.    A cookie is a small amount of data stored on the hard drive of the     individuals computer that allows Excel 4 Health Cluco-D to identify the     individual with his or her corresponding data that resides in Excel 4 Health Cluco-D s database. You may read more about cookies at     http://cookiecentral.com. Individuals who use the Websites need to     accept cookies in order to use all of the features and functionality of   the Websites. <br><br>  (b) Web Beacons.    A web beacon is programming code that can be used to display an image     on a web page by using an img src="x" programming function -- (see     http://truste.org for more information), but can also be used to     transfer an individuals unique user identification (often in the form     of a cookie) to a database and associate the individual with previously     acquired information about an individual in a database. This allows Excel 4 Health Cluco-D to track certain web sites an individual visits     online. Web beacons are used to determine products or services an     individual may be interested in, and to track online behavioral habits     for marketing purposes. For example, Excel 4 Health Cluco-D might place,     with the consent of a third party website, a web beacon on the third     partys website where fishing products are sold. When Joe, an individual    listed in Excel 4 Health Cluco-Ds database, visits the fishing website, Excel 4 Health Cluco-D receives notice by means of the web beacon that     Joe visited the fishing site, and Excel 4 Health Cluco-D would then     update Joes profile with the information that Joe is interested in     fishing. Excel 4 Health Cluco-D may thereafter present offers of fishing     related products and services to Joe. In addition to using web beacons     on web pages, Excel 4 Health Cluco-D also uses web beacons in email   messages sent to individuals listed in Excel 4 Health Cluco-Ds database. <br><br>  (c) New Technology.    The use of technology on the Internet, including cookies and web     beacons, is rapidly evolving, as is Excel 4 Health Cluco-Ds use of new     and evolving technology. As a result, Excel 4 Health Cluco-D strongly     encourages individuals to revisit this policy for any updates regarding   its use of technology. <br><br>  1.5 Outside Information. Excel 4 Health Cluco-D may receive information about individuals from     third parties or from other sources of information outside of Excel 4 Health Cluco-D including information located in public databases ("Outside  Information"). <br><br>  1.6 Individual Information.    As used herein, Individual Information means Survey Information, Third    Party List Information, Other Information, Technology Information, and     Outside Information, and any other information Excel 4 Health Cluco-D   gathers or receives about individuals. <br><br>  1.7 No Information Collected from Children. Excel 4 Health Cluco-D will never knowingly collect any personal     information about children under the age of 13. If Excel 4 Health Cluco-D    obtains actual knowledge that it has collected personal information     about a child under the age of 13, that information will be immediately     deleted from our database. Because it does not collect such information, Excel 4 Health Cluco-D has no such information to use or to disclose to     third parties. Excel 4 Health Cluco-D has designed this policy in order   to comply with the Childrens Online Privacy Protection Act ("COPPA"). <br><br>  1.8 Credit Card Information. Excel 4 Health Cluco-D may in certain cases collect credit card numbers    and related information, such as the expiration date of the card     ("Credit Card Information") when an individual places an order from Excel 4 Health Cluco-D. When the Credit Card Information is submitted to Excel 4 Health Cluco-D, such information is encrypted and is protected     with SSL encryption software. Excel 4 Health Cluco-D will use the Credit     Card Information for purposes of processing and completing the purchase     transaction . <br><br>  2. Use of Individual Information. <br><br>  2.1 Discretion to Use Information.    THE COMPANY MAY USE INDIVIDUAL INFORMATION FOR ANY LEGALLY PERMISSIBLE    PURPOSE IN COMPANYS SOLE DISCRETION.    The following paragraphs in Section 2 describe how Excel 4 Health Cluco-D currently uses Individual Information, but Excel 4 Health Cluco-D may change or broaden its use at any time. As noted below, Excel 4 Health Cluco-D may update this policy from time to time. Excel 4 Health Cluco-D may use Individual Information to provide promotional     offers to individuals by means of email advertising, telephone     marketing, direct mail marketing, online banner advertising, and package    stuffers, among other possible uses.    If you do not wish us to use personal information about you to promote    or sell our products and services or to sell, transfer or otherwise     provide personal information about you to third parties, please inform     us by contacting customer service at 844-946-0263 and we will certainly  honor your request.<br><br>   2.2 Email. Excel 4 Health Cluco-D uses Individual Information to provide     promotional offers by email to individuals. Excel 4 Health Cluco-D may     maintain separate email lists for different purposes. If email     recipients wish to end their email subscription from a particular list,     they need to follow the instructions at the end of each email message to  unsubscribe from the particular list. <br><br>  2.2(a) Content of Email Messages.    In certain commercial email messages sent by Excel 4 Health Cluco-D, an    advertisers name will appear in the "From:" line but hitting the     "Reply" button will cause a reply email to be sent to Excel 4 Health Cluco-D. The "Subject:" line of Excel 4 Health Cluco-D email messages   will usually contain a line provided from the advertiser to Excel 4 Health Cluco-D. <br><br>  2.2(b) Solicited Email. Excel 4 Health Cluco-D only sends email to individuals who have agreed     on the Websites to receive email from Excel 4 Health Cluco-D or to     individuals who have agreed on third party websites to receive email     from third parties such as Excel 4 Health Cluco-D Excel 4 Health Cluco-D     does not send unsolicited email messages. As a result, statutes   requiring certain formatting for unsolicited email are not applicable to Excel 4 Health Cluco-Ds email messages. <br><br>  2.3 Targeted Advertising. Excel 4 Health Cluco-D uses Individual Information to target     advertising to an individual. When an individual is using the Internet, Excel 4 Health Cluco-D uses Technology Information (see also Section 2.5     below) to associate an individual with that persons Individual     Information, and Excel 4 Health Cluco-D attempts to show advertising for     products and services in which the person has expressed an interest in     the Surveys, indicated an interest by means of Technology Information,     and otherwise. Excel 4 Health Cluco-D may, at its discretion, target     advertising by using email, direct mail, telephones, cell phones, and   other means of communication to provide promotional offers.<br><br>   2.4 Direct Mail and Telemarketing. Excel 4 Health Cluco-D uses Individual Information to advertise,     directly or indirectly, to individuals using direct mail marketing or   telemarketing using telephones and cell phones. <br><br>  2.5 Use of Technology Information. Excel 4 Health Cluco-D uses Technology Information (1) to match a     persons Survey Information and Third Party List Information to other     categories of Individual Information to make and improve profiles of     individuals, (2) to track a persons online browsing habits on the     Internet, (3) to determine which areas of Excel 4 Health Cluco-Ds web   sites are most frequently visited. This information helps Excel 4 Health Cluco-D to better understand the online habits of individuals so that Excel 4 Health Cluco-D can target advertising and promotions to them. <br><br>  2.6 Profiles of Individuals. Excel 4 Health Cluco-D uses Individual Information to make a profile of    an individual. A profile can be created by combining Survey Information    and Third Party List Information with other sources of Individual   Information such as information obtained from public databases. <br><br>  2.7 Storage of Individual Information. Excel 4 Health Cluco-D stores the Individual Information in a database     on Excel 4 Health Cluco-D computers. Our computers have security measures    (such as a firewall) in place to protect against the loss, misuse, and     alteration of the information under Excel 4 Health Cluco-Ds control. Not    withstanding such measures, Excel 4 Health Cluco-D cannot guarantee that    its security measures will prevent Excel 4 Health Cluco-D computers from    being illegally accessed, and the Individual Information on them stolen  or altered. <br><br>  3. Dissemination of Individual Information. <br><br>  3.1 Sale or Transfer to Third Parties. Excel 4 Health Cluco-D MAY SELL OR TRANSFER INDIVIDUAL INFORMATION TO     THIRD PARTIES FOR ANY PURPOSE IN Excel 4 Health Cluco-Ds SOLE     DISCRETION.    If you do not wish us to use personal information about you to promote    or sell our products and services or to sell, transfer or otherwise     provide personal information about you to third parties, please inform     us by contacting customer service at 844-946-0263  and we will   certainly honor your request. <br><br>  3.2 Order Fulfillment. Excel 4 Health Cluco-D will transfer Individual Information to third     parties when necessary to provide a product or service that a person     orders from such third party while using Excel 4 Health Cluco-D web sites  or when responding to offers provided by Excel 4 Health Cluco-D <br><br>  3.3 Legal Process. Excel 4 Health Cluco-D may disclose Individual Information to respond   to subpoenas, court orders, and other legal processes. <br><br>  3.4 Summary Data. Excel 4 Health Cluco-D may sell or transfer non-individualized     information, such as summary or aggregated anonymous information about   all persons or sub-groups of persons. <br><br>  3.5 Access.    Individuals have access to their Individual Information collected to     provide an opportunity for an individual to correct, amend, or delete     such information. Access can be obtained by contacting customer service     at the number on the order page. Excel 4 Health Cluco-D may also grant     advertising clients and email services providers’ access to an     individuals email address to verify the origin of the Individual   Information collected. <br><br>  4. Privacy Practices of Third Parties. <br><br>  4.1 Advertiser cookies and web beacons.    Advertising agencies, advertising networks, and other companies     (together, "Advertisers") who place advertisements on the Websites and     on the Internet generally may use their own cookies, web beacons, and     other technology to collect information about individuals. Excel 4 Health Cluco-D does not control Advertisers use of such technology and Excel 4 Health Cluco-D has no responsibility for the use of such   technology to gather information about individuals. <br><br>  4.2 Links.    The Websites and email messages sometimes contain hypertext links to     the web sites of third parties. Excel 4 Health Cluco-D is not responsible    for the privacy practices or the content of such other web sites.     Linked web sites may contain links to web sites maintained by third     parties. Such links are provided for your convenience and reference     only. Excel 4 Health Cluco-D does not operate or control in any respect     any information, software, products or services available on such third     party web sites. The inclusion of a link to a web site does not imply     any endorsement of the services or the site, its contents, or its   sponsoring organization. <br><br>   <br><br>  5. Unsubscribe Procedures.    If you wish to discontinue receiving email messages from Excel 4 Health Cluco-D, please call our customer service line at 844-946-0263 .    We reserve the right to add Individual Information to multiple lists   maintained by Excel 4 Health Cluco-D<br><br>  If you have questions about this policy, please feel free to contact     us at  844-946-0263 </p></div>
      </div>


    </div>
  </div>
</div>

</html>