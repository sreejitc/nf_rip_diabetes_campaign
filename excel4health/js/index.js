var ll = jQuery.noConflict();
var currencySymbol = 'll';



//variable used to enable or disbale popup
var internalLink = false;
var cboxOptions = {
					  width: '95%',
					  height: '95%',
					  maxWidth: '960px',
					  maxHeight: '960px',
					}
/* **************************************************functions *****************************************************************/

/*
    * AlertHandler allows you to either use a standard alert to show errors or customize to some kind of HTML version of your own
    */
   function AlertHandler(message)
   {
      alert(message);
   }

function loadingTextGif(){
	var counter=-1;
	ll("#exitOverlay").css('height', ll( document ).height());
	var loadingGif=document.getElementById('delivery');
	var timer = setInterval(function(){
		counter++;
		loadingTextTimer(counter);
	}, 5000);
	loadingGif.style.display = "block";
}
function loadingTextTimer(i){
	var loadingGif=document.getElementById('delivery');
	var messages = ["This may take up to 30 seconds.", "Please stand by…", "Checking product availability… ", "Validating order details…","Authorizing payment…","Completing order processing…"];
	var deliveryM = loadingGif.getElementsByTagName('p')[0];
	if(i>4){
		i=5;
	}
	deliveryM.innerHTML=messages[i];
}
function popup(url){
	cuteLittleWindow = window.open(url, "littleWindow", "location=no,width=620,height=800");
}

function freeShipping() {
	ll('#discountholder').css('display','block');

	if(localStorage.getItem("popStep") == null) {
		// ll("#pop-up").css("opacity",0);
		// ll("#pop-up").hide();

		// localStorage.setItem("popStep", "1");
		// ll("#discountBannerPrice").html("$23.47");
	}
	else if(localStorage.getItem("popStep") == "1") {
		// ll("#pop-up2").css("opacity",0);
		// ll("#pop-up2").hide();

		// localStorage.setItem("popStep", "2");
		// ll("#discount_active").val("1");
		// ll("#discountBannerPrice").html("$4.95");

		// ll.ajax({
		// 	url:'includes/discount.php',
		// 	type:'POST',
		// 	data: 'mystep=' + 'activate_discount'
		// }).done(function(data){});
   }

	window.scrollTo(0,0);
}

/*function createLLProspect(){
	ll.ajax({
			url:'includes/newprospect.php',
			type:'POST',
			data: 'fields_fname=' + ll('#fields_fname').val() +
				  '&fields_lname=' + ll('#fields_lname').val() +
				  '&fields_address1=' + ll('#fields_address1').val() +
				  '&fields_city=' + ll('#fields_city').val() +
				  '&fields_state=' + ll('#fields_state').val() +
				  '&fields_zip=' + ll('#fields_zip').val() +
				  '&fields_email=' + ll('#fields_email').val() +
				  '&fields_phone=' + ll('#fields_phone').val() +
				  '&campaign_id=' + ll('#campaign_id').val() +
				  '&AFID=' + ll('#AFID').val() +
				  '&SID=' + ll('#SID').val() +
				  '&AFFID=' + ll('#AFFID').val() +
				  '&C1=' + ll('#C1').val() +
				  '&C2=' + ll('#C2').val() +
				  '&C3=' + ll('#C3').val()
		});
}*/

function message(dom,content){
	dom.html("<span>"+content+"</span>");
}


function getUrlVars()
{
    var result = [], hash;
    var y = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < y.length; i++)
    {
        hash = y[i].split('=');
        result.push(hash[0]);
        result[hash[0]] = hash[1];
    }
    return result;
}
function urlParam(e,t){t=t||window;var n=new RegExp("[&|?]"+e+"=([^&#]+)","i"),r=t.document.URL.match(n);return r?r[1]:""}
/* **************************************************functions *****************************************************************/


ll(document).scroll(function () {
	ll.colorbox.resize({
		width: window.innerWidth > parseInt(cboxOptions.maxWidth) ? cboxOptions.maxWidth : cboxOptions.width,
		height: window.innerHeight > parseInt(cboxOptions.maxHeight) ? cboxOptions.maxHeight : cboxOptions.height
	  });
});
ll(document).ready(function(){
	ll("#pop-up").hide();
	ll("#pop-up2").hide();
	ll("#pop-up3").hide();
	var obj=getUrlVars();
	if(
	(("special" in obj) && (obj["special"].substring(0,8)=="discount"||obj["special"].substring(0,8)=="DISCOUNT"))||
	(("SPECIAL" in obj) && (obj["SPECIAL"].substring(0,8)=="DISCOUNT"))
	){
		freeShipping();
	}
	ll('a.fancybox').click(function(e){
		e.preventDefault();
		var url=ll(this).attr('href');
		ll.colorbox({
			width: cboxOptions.width,
			  height: cboxOptions.height,
			  maxWidth: cboxOptions.maxWidth,
			  maxHeight: cboxOptions.maxHeight,
			href:url
			});
		return false;
	});
	ll(window).resize(function(){
	  ll.colorbox.resize({
		width: window.innerWidth > parseInt(cboxOptions.maxWidth) ? cboxOptions.maxWidth : cboxOptions.width,
		height: window.innerHeight > parseInt(cboxOptions.maxHeight) ? cboxOptions.maxHeight : cboxOptions.height
	  });
	});
	/*ll('#fields_phone').mask('(999)999-9999');
	ll('#fields_zip').mask('99999')
	ll('#fields_phone').blur(function(){
		createLLProspect();
	});*/

	//disabling pop-ups on certain page exit events
	ll('a').click(function(){
		if(ll(this).attr('id') =="exitLink" || ll(this).attr('id') =="exitLink2"|| ll(this).attr('id') =="showReview"){
			window.internalLink = false;
		}
		else{
			window.internalLink = true;
		}
   });

   if ("hide" == urlParam("exit") || "no" == urlParam("exit") || "0" == urlParam("exit") || "false" == urlParam("exit") || "n" == urlParam("exit")) {
      window.internalLink = true;
      localStorage.setItem("popStep", "0");
		ll("#discount_active").val("0");
   }

	ll('form[name="opt_in_form"]').submit(function(){
		   window.internalLink = true;
	});
    ll('#lp_button_1').click(function(){
    	window.internalLink = true;
	});


	//end disabling pop-ups on certain page exit events

	/*window.setTimeout(function() {
			// pop up function being tied to the windows unload event
			var exitMessage = "****************************************************************************************\n\WAIT! WAIT! WAIT! WAIT! WAIT! WAIT! WAIT! WAIT! WAIT!  \n\nFor a Limited Time - Check Out our SPECIAL OFFER!! \n\nIf you are wondering why we are doing this, the simple answer is because we know once you see results, you will refer friends and family. \n\nDon't Miss Out on this LIMITED TIME SPECIAL OFFER!\n\nClick *Cancel* or *Stay* below to activate offer.\n\n****************************************************************************************";
			if(exitMessage){
				function pageUnload() {
					if (!window.internalLink) {
						// if(localStorage.getItem("popStep") == null) {
						// 	ll("#pop-up").show();
						// 	ll("#pop-up").animate({ opacity: 1 });
						// }
                  if(localStorage.getItem("popEbook") != "1") {
							// ll("#pop-up2").show();
							// ll("#pop-up2").animate({ opacity: 1 });

                     ll("#pop-up3").show();
							ll("#pop-up3").animate({ opacity: 1 });
							window.onbeforeunload = null;
           		      window.removeEventListener("mouseout", mouseoutHandler, false);

                     localStorage.setItem("popEbook", "1");
						}
          	      else if(localStorage.getItem("popStep") == "2") {
							// ll("#pop-up3").show();
							// ll("#pop-up3").animate({ opacity: 1 });
							// window.onbeforeunload = null;
           		      // window.removeEventListener("mouseout", mouseoutHandler, false);
						}
						return exitMessage;
					}
				}

				window.onbeforeunload = pageUnload;

				var mouseY = 0;
				var topValue = 0;
				var mouseoutHandler = function(e) {
         	mouseY = e.clientY;
					if(mouseY < topValue) {
						return pageUnload();
					}
        };
				window.addEventListener("mouseout", mouseoutHandler, false);
			}
	}, 5000);*/

		//end pop up function being tied to the windows unload event
		ll('.popclose').on('click', function(e) {
			ll('#pop-up3').hide();
		});

		//--- Exit Popup ------------------------
		ll('#exitPopupBtn').on('click', function(e){
			e.preventDefault();
         createMaropostProspect();
    });
    
    /*ll('#fields_zip').blur(function(){
      var postalCode = ll(this).val();
      var city = '';
      var province = '';
  
      //make a request to the google geocode api
      ll.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+postalCode).done(function(response){
        //find the city and province
        var address_components = response.results[0].address_components;
        ll.each(address_components, function(index, component){
          var types = component.types;
          ll.each(types, function(index, type){
            if(type == 'locality') {
              city = component.long_name;
            }
            if(type == 'administrative_area_level_1') {
              province = component.short_name;
            }
          });
        });
  
        //pre-fill the city and province
        ll('#fields_city').val(city);
        ll('#fields_state').val(province);
      });
    });*/

   var clock = ll('.clock').FlipClock(600, {
      countdown: true
   });

   ll("#scarcityFade p").hide();
   function fades($div, cb) {
      $div.fadeIn(2000, function () {
         $div.fadeOut(2000, function () {
            var $next = $div.next();
            if ($next.length > 0) {
               fades($next, cb);
            }
            else {
               cb();
            }
         });
      });
   }
      
   function startFading($firstDiv) {
      fades($firstDiv, function () {
         startFading($firstDiv);
      });
   }

   startFading(ll("#scarcityFade p:first-child"));

});
