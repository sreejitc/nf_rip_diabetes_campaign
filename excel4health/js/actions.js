function openNewWindow( o, l, a, e, d, m, n, p ) {
			if ( o || ( o = "cvv.php" ), l || ( l = "modal" ), e || ( e = 480 ), d || ( d = 480 ), m || ( m = 50 ), n || ( n = 50 ), p || ( p = "resizable,scrollbars" ), "popup" == l ) {
				var i = "height=" + d + ",";
				i += "width=" + e + ",", i += "top=" + m + ",", i += "left=" + n + ",", i += p, win = window.open( o, a, i ), win.window.focus()
			} else if ( "modal" == l ) {
				ll( "body" ).addClass( "noflow" );
				var c = "";
				c += '<div id="app_common_modal">', c += '<div class="app_modal_body"><a href="javascript:void(0);" id="app_common_modal_close">X</a><iframe src="' + o + '" frameborder="0" ></iframe></div>', c += "</div>", ll( "#app_common_modal" ).length || ll( "body" ).append( c ), ll( "#app_common_modal" ).fadeIn()
			}
		}
		ll( document ).on( "click", "#app_common_modal_close", function ( o ) {
			o.preventDefault ? o.preventDefault() : o.returnValue = !1, ll( "body" ).removeClass( "noflow" ), ll( "#app_common_modal" ).remove()
		} );